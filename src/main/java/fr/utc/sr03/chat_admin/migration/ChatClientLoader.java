package fr.utc.sr03.chat_admin.migration;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ChatClientLoader implements CommandLineRunner {

    private final ChatClientRepository repository;

    @Autowired
    public ChatClientLoader(ChatClientRepository repository) {
        this.repository = repository;
    }

    @Override
    public void run(String... strings) throws Exception {

    }
    }