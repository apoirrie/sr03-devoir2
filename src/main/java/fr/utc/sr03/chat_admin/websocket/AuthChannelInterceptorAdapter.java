package fr.utc.sr03.chat_admin.websocket;

import fr.utc.sr03.chat_admin.security.JWTUtil;
import fr.utc.sr03.chat_admin.services.UserService;
import io.jsonwebtoken.Claims;
import jakarta.security.auth.message.AuthException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
public class AuthChannelInterceptorAdapter implements ChannelInterceptor {
    private final JWTUtil jwtTokenUtil;
    private final UserService userService;
    @Override
    public Message<?> preSend(@NonNull Message<?> message, @NonNull MessageChannel channel) {
        StompHeaderAccessor accessor =
                MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);


        assert accessor != null;
        if (StompCommand.CONNECT.equals(accessor.getCommand())) {
            String authorizationHeader = accessor.getFirstNativeHeader("Authorization");
            try {
                String accessToken = jwtTokenUtil.resolveToken(authorizationHeader);
                assert accessToken != null;
                Claims claims = jwtTokenUtil.resolveClaimsForWs(accessToken);
                if(claims != null & jwtTokenUtil.validateClaims(claims)){
                    String email = claims.getSubject();
                    UserDetails user = userService.loadUserByUsername(email);
                    Authentication authentication =
                            new UsernamePasswordAuthenticationToken(user.getUsername(),null,user.getAuthorities());

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    accessor.setUser(authentication);
                    Map<String, Object> sessionAttributes = new HashMap<>();
                }
                else {
                    throw new AuthException("Authentification failed");
                }

            }catch (Exception e){
                log.error(e.getMessage());
                throw new RuntimeException("Authentification failed");


            }
            return message;

        }

        return message;
    }


}
