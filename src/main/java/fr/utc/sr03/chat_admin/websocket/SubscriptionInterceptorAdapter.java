package fr.utc.sr03.chat_admin.websocket;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.security.JWTUtil;
import fr.utc.sr03.chat_admin.services.ChatService;
import fr.utc.sr03.chat_admin.services.UserService;
import io.jsonwebtoken.Claims;
import jakarta.security.auth.message.AuthException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Objects;


@Component
@RequiredArgsConstructor
@Slf4j
public class SubscriptionInterceptorAdapter implements ChannelInterceptor {
    private final ChatService chatService;
    @Override
    public Message<?> preSend(@NonNull Message<?> message, @NonNull MessageChannel channel) {
        StompHeaderAccessor accessor =
                MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        assert accessor != null;
        if (StompCommand.SUBSCRIBE.equals(accessor.getCommand()) ||StompCommand.MESSAGE.equals(accessor.getCommand()) ) {
            String destination = accessor.getDestination();
            if(destination != null && destination.startsWith("/topic/messages"))
            {
                Long id = chatService.extractChatId(destination);
                if(id == null || !chatService.canSubscribeToChat(id, accessor.getUser().getName()))
                {
                    throw new RuntimeException("You dont have access to this chat");
                }
            }

        }

        return message;
    }


}
