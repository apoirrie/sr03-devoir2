package fr.utc.sr03.chat_admin.websocket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.socket.messaging.StompSubProtocolErrorHandler;

@Slf4j
public class StompErrorHandler extends StompSubProtocolErrorHandler {

    @Override
    public Message<byte[]> handleClientMessageProcessingError(Message<byte[]> clientMessage, Throwable ex) {
        StompHeaderAccessor accessor = StompHeaderAccessor.create(StompCommand.ERROR);
        if(ex.getCause().getMessage().equals("Authentification failed"))
        {

            accessor.setMessage("Authentification failed");
            accessor.setLeaveMutable(true);

            return MessageBuilder.createMessage("Unable to authentificate".getBytes(), accessor.getMessageHeaders());
        }
        return MessageBuilder.createMessage(ex.getCause().getMessage().getBytes(), accessor.getMessageHeaders());
    }


}
