package fr.utc.sr03.chat_admin.chat;


import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;

import fr.utc.sr03.chat_admin.model.LightChatClient;
import fr.utc.sr03.chat_admin.services.ChatService;
import fr.utc.sr03.chat_admin.services.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.data.repository.query.Param;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Controller
@RequiredArgsConstructor
@Slf4j
public class ChatEventController {
    private final ChatService chatService;
    private final UserService userService;
    private final ChatClientRepository chatClientRepository;
    private final RoomController roomController;
    private Map<String,String> subscribeMap = new HashMap<>();
    private Map<String, LightChatClient> usersMap = new HashMap<>();
    @EventListener
    private void handleSessionSubscribeEvent(SessionSubscribeEvent event) {

        log.info(event.toString());
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        assert accessor.getDestination() != null;
        if(accessor.getDestination().startsWith("/topic/users"))
        {
            subscribeMap.put(accessor.getSubscriptionId(), accessor.getDestination());
            Long chatId = chatService.extractChatId(accessor.getDestination());
            assert chatId != null;
            roomController.addClientToRoom(chatId,usersMap.get(accessor.getSessionId()));
        } else if (accessor.getDestination().startsWith("/topic/messages")) {
            subscribeMap.put(accessor.getSubscriptionId(), accessor.getDestination());
        }

    }
    @EventListener
    private void handlerSessionConnectEvent(SessionConnectedEvent event)
    {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        Optional<ChatClient> user = chatClientRepository.findByEmail(Objects.requireNonNull(accessor.getUser()).getName());
        Optional<LightChatClient> client = usersMap.values().stream().filter(c -> c.getId().equals(user.get().getId())).findFirst();
        if(client.isPresent())
        {
            roomController.removeClientFromAllRooms(client.get());
            for (Map.Entry<String, LightChatClient> entry : usersMap.entrySet()) {
                if (entry.getValue().getId().equals(client.get().getId())) {
                   usersMap.remove(entry.getKey());
                   break;
                }
            }
        }
        usersMap.put(accessor.getSessionId(),userService.toLightChatClient(user.get()));
    }
    @EventListener
    private void handlerSessionDisconnectEvent(SessionDisconnectEvent event)
    {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        LightChatClient user = usersMap.get(accessor.getSessionId());
        if(user != null)
        {
            roomController.removeClientFromAllRooms(user);
            usersMap.remove(accessor.getSessionId());
        }
    }
    @EventListener
    private void handleSessionUnsubscribeEvent(SessionUnsubscribeEvent event)
    {
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        if(subscribeMap.containsKey(accessor.getSubscriptionId()))
        {
            if(subscribeMap.get(accessor.getSubscriptionId()).startsWith("/topic/users"))
            {
                Long chatId = chatService.extractChatId(subscribeMap.get(accessor.getSubscriptionId()));
                assert chatId != null;
                Optional<ChatClient> user = chatClientRepository.findByEmail(Objects.requireNonNull(accessor.getUser()).getName());
                assert user.isPresent();
                roomController.removeClientFromRoom(chatId,usersMap.get(accessor.getSessionId()));

            }
            subscribeMap.remove(accessor.getSubscriptionId());

        }
    }

    @MessageMapping("/send/{chatId}")
    public void onMessage(@DestinationVariable String chatId, Message<String> message)
    {
        StompHeaderAccessor accessor =
                MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);
        assert accessor != null;
        Optional<ChatClient> user = chatClientRepository.findByEmail(Objects.requireNonNull(accessor.getUser()).getName());
        roomController.sendMessage(Long.valueOf(chatId),user.get().getId(),message.getPayload());
    }

}
