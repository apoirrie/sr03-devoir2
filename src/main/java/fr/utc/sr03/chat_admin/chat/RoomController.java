package fr.utc.sr03.chat_admin.chat;


import fr.utc.sr03.chat_admin.model.LightChatClient;
import fr.utc.sr03.chat_admin.model.Room;
import fr.utc.sr03.chat_admin.model.websocket.ChatMessage;
import fr.utc.sr03.chat_admin.model.websocket.MessageType;
import fr.utc.sr03.chat_admin.model.websocket.UserListMessage;
import fr.utc.sr03.chat_admin.services.ChatService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Slf4j
@Controller
public class RoomController {

    private HashMap<Long,Room> rooms = new HashMap<Long,Room>();
    private final SimpMessagingTemplate messagingTemplate;
    @Qualifier("clientOutboundChannel")
    private final MessageChannel clientOutboundChannel;

    public RoomController(@Qualifier("clientOutboundChannel") MessageChannel clientOutboundChannel,SimpMessagingTemplate messagingTemplate)
    {
        this.messagingTemplate = messagingTemplate;
        this.clientOutboundChannel = clientOutboundChannel;
    }
    public void addClientToRoom(Long chatId, LightChatClient client)
    {
        if(!rooms.containsKey(chatId))
        {
            rooms.put(chatId, new Room(chatId));
        }
        rooms.get(chatId).addConnectedUser(client);
        sendUserUpdateMessage(chatId,client,true);
        sendUsersListMessage(chatId);
    }

    public void removeClientFromRoom(Long chatId, LightChatClient client)
    {
        rooms.get(chatId).removeConnectedUser(client.getId());
        sendUserUpdateMessage(chatId,client,false);
        sendUsersListMessage(chatId);
    }

    public void removeClientFromAllRooms(LightChatClient client)
    {
        for(Room room : rooms.values())
        {
            if(room.hasUser(client.getId()))
            {
                room.removeConnectedUser(client.getId());
                sendUserUpdateMessage(room.getChatId(),client,false);
                sendUsersListMessage(room.getChatId());
            }
        }
    }

    private void sendUsersListMessage(Long chatId)
    {
        UserListMessage message = UserListMessage.builder()
                .users(rooms.get(chatId).getConnectedUsers())
                .date(new Date())
                .build();
        messagingTemplate.convertAndSend("/topic/users/" + chatId.toString(), message);
    }


    private void sendUserUpdateMessage(Long chatId, LightChatClient user,boolean joined)
    {
        ChatMessage message = ChatMessage.builder()
                .senderId(user.getId())
                .type(joined ? MessageType.JOIN : MessageType.LEAVE)
                .payload(user.getName() + " " + user.getSurname())
                .date(new Date()).build();
        messagingTemplate.convertAndSend("/topic/messages/" + chatId.toString(), message);
    }
    public void sendMessage(Long chatId, Long userId, String payload)
    {
        ChatMessage message = ChatMessage.builder()
                .senderId(userId)
                .type(MessageType.MESSAGE)
                .payload(payload)
                .date(new Date()).build();
        messagingTemplate.convertAndSend("/topic/messages/" + chatId.toString(), message);
    }


    @MessageExceptionHandler
    public void handleAccessDeniedException(AccessDeniedException ex, StompHeaderAccessor accessor) {
        log.error(ex.getMessage());
        StompHeaderAccessor accessorA = StompHeaderAccessor.create(StompCommand.MESSAGE);
        accessorA.setSessionId(accessor.getSessionId());
        clientOutboundChannel.send(MessageBuilder.createMessage(ex.getMessage().getBytes(),accessorA.getMessageHeaders()));
    }



}
