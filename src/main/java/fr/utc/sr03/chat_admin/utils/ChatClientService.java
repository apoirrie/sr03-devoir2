package fr.utc.sr03.chat_admin.utils;

import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.LightChatClient;
import org.springframework.stereotype.Service;

@Service
public class ChatClientService {


    public LightChatClient convertToLight(ChatClient client)
    {
        return new LightChatClient(client.getId(),client.getEmail(),client.getName(),client.getSurname());
    }

}
