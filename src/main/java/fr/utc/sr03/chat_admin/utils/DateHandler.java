package fr.utc.sr03.chat_admin.utils;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

@Service
public class DateHandler {


    final String dateString = "2024-06-25T22:00:00.000Z";
    final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public DateHandler()
    {
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public Optional<Date> dayJsStringToDate(String stringifiedDate)
    {
        try {
            Date date = formatter.parse(stringifiedDate);
            return Optional.ofNullable(date);
        } catch (ParseException e) {
            return Optional.empty();
        }
    }
}
