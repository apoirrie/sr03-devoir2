package fr.utc.sr03.chat_admin.security;

import fr.utc.sr03.chat_admin.config.RsaKeyProperties;
import fr.utc.sr03.chat_admin.services.UserService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.savedrequest.NullRequestCache;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableMethodSecurity
@EnableWebSecurity
public class SecurityConfig {
    private final UserService userService;
    private final JwtAuthorizationFilter jwtAuthorizationFilter;
    public SecurityConfig(UserService userService,JwtAuthorizationFilter jwtAuthorizationFilter) {
        this.userService = userService;
        this.jwtAuthorizationFilter = jwtAuthorizationFilter;
    }

    @Bean
    @Order(1)
    public SecurityFilterChain adminFilterChain(HttpSecurity http) throws Exception {
        return http.securityMatcher("/admin/**","/static/**","/html/**","/login", "/signup", "/logout")
                .authorizeHttpRequests(auth -> {
            auth.requestMatchers("/admin/**").hasRole("ADMIN")
                    .
                    requestMatchers("/static/**","/html/**", "/signup", "/logout").permitAll()
                    .anyRequest().authenticated();
        })
                .formLogin(formLogin -> formLogin.usernameParameter("email").loginPage("/login").permitAll().defaultSuccessUrl("/admin/accueil", true))
                .logout(logout -> logout.logoutSuccessUrl("/login").logoutUrl("/logout").permitAll())
                .build();
    }

    @Bean
    public AuthenticationManager authenticationManager(HttpSecurity http)
            throws Exception {
        AuthenticationManagerBuilder authenticationManagerBuilder = http.getSharedObject(AuthenticationManagerBuilder.class);
        authenticationManagerBuilder.userDetailsService(userService).passwordEncoder(passwordEncoder());
        return authenticationManagerBuilder.build();
    }

    @Bean
    @Order(2)
    public SecurityFilterChain restFilterChain(HttpSecurity http, AuthenticationManager authenticationManager) throws Exception {
        return http.securityMatcher("/api/**")
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(auth -> {
                    auth.
                    requestMatchers("/api/authentificate").permitAll()
                            .anyRequest().authenticated();
                })
                .csrf(AbstractHttpConfigurer::disable)
                .authenticationManager(authenticationManager)
                .formLogin(AbstractHttpConfigurer::disable)
                .addFilterBefore(jwtAuthorizationFilter,UsernamePasswordAuthenticationFilter.class)
                .build();
    }

    @Bean
    @Order(3)
    public SecurityFilterChain webSocketFilterChain(HttpSecurity http, AuthenticationManager authenticationManager) throws Exception {
        return http.securityMatcher("/ws", "ws/**")
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(auth -> {
                    auth.requestMatchers("/ws","ws/**").permitAll();
                })
                .csrf(AbstractHttpConfigurer::disable)
                .build();
    }



    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(8);
    }








}
