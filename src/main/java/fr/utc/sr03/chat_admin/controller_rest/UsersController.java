package fr.utc.sr03.chat_admin.controller_rest;


import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.LightChatClient;
import fr.utc.sr03.chat_admin.model.reponses.GenericReponseError;
import fr.utc.sr03.chat_admin.model.reponses.GetMeRes;
import fr.utc.sr03.chat_admin.model.reponses.GetUsersRes;
import fr.utc.sr03.chat_admin.services.UserService;
import fr.utc.sr03.chat_admin.utils.ChatClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@RestController
public class UsersController {

    private final ChatClientService chatClientService;
    private final ChatClientRepository chatClientRepository;
    private final UserService userService;


    @ResponseBody
    @RequestMapping(value = "/api/me",method = RequestMethod.GET)
    private ResponseEntity<?> getMe()
    {
        //TODO get only active users
        Optional<ChatClient> client = chatClientRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        if(client.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GenericReponseError("User not found"));
        }
        return ResponseEntity.ok(new GetMeRes(chatClientService.convertToLight(client.get())));
    }

    @ResponseBody
    @RequestMapping(value = "/api/getactiveusers",method = RequestMethod.GET)
    private ResponseEntity<GetUsersRes> getUsers()
    {
        //TODO get only active users
        List<ChatClient> clients = chatClientRepository.findAll();
        List<LightChatClient> lightClients = clients
                .stream()
                .map(userService::toLightChatClient)
                .toList();
        return ResponseEntity.ok(new GetUsersRes(lightClients));
    }
}
