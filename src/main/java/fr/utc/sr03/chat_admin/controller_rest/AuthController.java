package fr.utc.sr03.chat_admin.controller_rest;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.reponses.AuthRes;
import fr.utc.sr03.chat_admin.model.ErrorRes;
import fr.utc.sr03.chat_admin.model.LoginReq;
import fr.utc.sr03.chat_admin.security.JWTUtil;
import fr.utc.sr03.chat_admin.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AuthController {
    private final AuthenticationManager authenticationManager;

    private final ChatClientRepository chatClientRepository;
    private final UserService userService;
    private final JWTUtil jwtUtil;


    @ResponseBody
    @RequestMapping(value = "/api/authentificate",method = RequestMethod.POST)
    public ResponseEntity login(@Valid @RequestBody LoginReq loginReq)  {
        log.info(loginReq.getEmail());
        log.info(loginReq.getPassword());
        try {
            Authentication authentication =
                    authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginReq.getEmail(), loginReq.getPassword()));
            String email = authentication.getName();
            User user = new User(authentication.getName(),"", authentication.getAuthorities());
            String token = jwtUtil.createToken(user);
            AuthRes loginRes = new AuthRes(email,token);

            return ResponseEntity.ok(loginRes);

        }catch (BadCredentialsException e){
            userService.handleWrongConnectionAttemp(loginReq.getEmail());

            ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST,"Invalid username or password");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }catch (Exception e){
            ErrorRes errorResponse = new ErrorRes(HttpStatus.BAD_REQUEST, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }
}
