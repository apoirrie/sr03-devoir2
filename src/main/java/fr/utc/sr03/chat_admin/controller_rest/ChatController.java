package fr.utc.sr03.chat_admin.controller_rest;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.dao.ChatRepository;
import fr.utc.sr03.chat_admin.model.Chat;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.CreateChatBody;
import fr.utc.sr03.chat_admin.model.reponses.CreateChatRes;
import fr.utc.sr03.chat_admin.model.reponses.GenericReponseError;
import fr.utc.sr03.chat_admin.model.reponses.GetChatsRes;
import fr.utc.sr03.chat_admin.services.ChatService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@RestController
public class ChatController {

    private final ChatService chatservice;
    private final ChatRepository repository;
    private final ChatClientRepository chatClientRepository;

    @ResponseBody
    @RequestMapping(value = "/api/createchat",method = RequestMethod.POST)
    public ResponseEntity createChat(@Valid @RequestBody CreateChatBody createChatBody)
    {
        try{
            Optional<ChatClient> client = chatClientRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
            //TODO check user is active and exist
            Long id =chatservice.createChat(createChatBody,client.get());
            return ResponseEntity.ok(new CreateChatRes(id));
        }catch (Exception e)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GenericReponseError(e.getMessage()));
        }

    }
    @RequestMapping(value = "/api/getchat/{chatId}")
    public ResponseEntity getChat(@PathVariable Long chatId)
    {
        Optional<Chat>  chat =  repository.findChatById(chatId);
        if(chat.isEmpty())
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericReponseError("no chat with this id"));
        }
        return ResponseEntity.ok(chat.get());
    }

    @RequestMapping(value = "/api/editchat/{chatId}")
    public ResponseEntity editChat(@PathVariable Long chatId,@Valid @RequestBody CreateChatBody createChatBody) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            assert auth != null;
            Optional<ChatClient> client = chatClientRepository.findByEmail(auth.getName());
            assert client.isPresent();
            List<Chat> chats = repository.findChatsByCreatorId(client.get().getId());
            Chat findedChat = null;
            for(Chat chat : chats)
            {
                if(chat.getId().equals(chatId))
                {
                    findedChat = chat;
                    break;
                }
            }
            if(findedChat == null)
            {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(new GenericReponseError("chat is not owned by you, you cannot edit it"));
            }
            chatservice.editChat(createChatBody, findedChat);
            return ResponseEntity.ok().build();
        } catch (Exception e)
        {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericReponseError(e.getMessage()));
    }

    }

    @ResponseBody
    @DeleteMapping("/api/deletechat/{chatId}")
    public ResponseEntity deleteChat(@PathVariable Long chatId) {
        try {
            chatservice.deleteChat(chatId);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new GenericReponseError(e.getMessage()));
        }
    }

    @ResponseBody
    @RequestMapping(value = "/api/getmychats",method = RequestMethod.POST)
    public ResponseEntity getChats()
    {
        try{
            Optional<ChatClient> client = chatClientRepository.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
            //TODO check user is active and exist

            return ResponseEntity.ok(new GetChatsRes(chatservice.getChatOfUser(client.get())));
        }catch (Exception e)
        {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new GenericReponseError(e.getMessage()));
        }

    }
}
