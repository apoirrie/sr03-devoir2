package fr.utc.sr03.chat_admin.dao;

import fr.utc.sr03.chat_admin.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@Repository
public interface ChatRepository extends JpaRepository<Chat, Long> {

    Optional<Chat> findChatByName(String name);

    Optional<Chat> findChatById(Long id);


    List<Chat> findChatsByCreatorId(Long id);

    boolean existsChatByIdAndParticipantsIdsContainingAndExpirationIsAfterAndCreationIsBefore(Long chatId, Long participantId, Date date1,Date date);
    List<Chat> findByParticipantsIdsContaining(Long participantId);
    boolean existsByName(String name);

}
