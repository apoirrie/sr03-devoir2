package fr.utc.sr03.chat_admin.dao;

import fr.utc.sr03.chat_admin.model.ChatClient;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface ChatClientRepository extends JpaRepository<ChatClient, Long> {
    Optional<ChatClient> findByEmail(String email);
    List<ChatClient> findByStatus(String status);
    Page<ChatClient> findByStatus(String status, Pageable pageable);

}




