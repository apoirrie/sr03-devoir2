package fr.utc.sr03.chat_admin.dao;

import fr.utc.sr03.chat_admin.model.AttempConnectionModel;
import fr.utc.sr03.chat_admin.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConnectionAttempRepository extends JpaRepository<AttempConnectionModel, Long> {

    Optional<AttempConnectionModel> getAttempConnectionModelByClientId(Long clientId);

}
