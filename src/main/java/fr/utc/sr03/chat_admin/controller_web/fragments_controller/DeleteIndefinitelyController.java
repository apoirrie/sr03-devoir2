package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.EditChatClientBody;
import jakarta.validation.Valid;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Optional;
@Controller
public class DeleteIndefinitelyController extends AbstractFragmentController {


    ChatClientRepository repo;
    private final PasswordEncoder encoder;
    private Long userId;
    public DeleteIndefinitelyController(PasswordEncoder encoder, ChatClientRepository repo) {
        super("delete_indefinitely", "Utilisateur supprimé");
        this.encoder = encoder;
        this.repo = repo;
    }

    @GetMapping("/admin/delete_indefinitely/{id}")
    public String deleteUserIndefinitely(@PathVariable Long id, Model model) {
        userId=id;
        Optional<ChatClient> client = repo.findById(userId);
        if (client.isPresent()) {
            EditChatClientBody editClientBody = new EditChatClientBody();
            ChatClient existingClient = client.get();
            editClientBody.setName(existingClient.getName());
            editClientBody.setSurname(existingClient.getSurname());
            editClientBody.setEmail(existingClient.getEmail());
            editClientBody.setRole(existingClient.getRole().equals("ADMIN"));
            model.addAttribute("editClient", editClientBody);
            Optional<ChatClient> userToDeleteIndefinitely = repo.findById(userId);
            model.addAttribute("userToDeleteIndefinitely", userToDeleteIndefinitely.orElse(null));
            return "fragments/delete_indefinitely";
        } else {
            return "redirect:/admin/accueil";
        }
    }

    @PostMapping("/admin/delete_indefinitely/{id}")
    public String deleteUserIndefinitely(@PathVariable Long id, @Valid @ModelAttribute("editClient") EditChatClientBody editClient, RedirectAttributes attributes, BindingResult validationResult) {
        if (validationResult.hasErrors()) {
            return "redirect:/admin/accueil";
        }
        try {
            Optional<ChatClient> existingClientOptional = repo.findById(userId);
            if (existingClientOptional.isPresent()) {
                ChatClient existingClient = existingClientOptional.get();
                existingClient.setName(editClient.getName());
                existingClient.setSurname(editClient.getSurname());
                existingClient.setEmail(editClient.getEmail());
                existingClient.setPassword(encoder.encode(editClient.getPassword()));
                existingClient.setRole(editClient.getRole() ? "ADMIN" : "USER");
                existingClient.setStatus("SUPPRIME");
                repo.delete(existingClient);
            } else {
                repo.save(new ChatClient(editClient.getName(), editClient.getSurname(), editClient.getEmail(), encoder.encode(editClient.getPassword()), editClient.getRole() ? "ADMIN" : "USER", "SUPPRIME"));
                attributes.addFlashAttribute("success", "User deleted !");

            }
        }
        catch(Exception e)
        {
            attributes.addFlashAttribute("error", e.getMessage());
        }


        return "redirect:/admin/accueil";
    }
    @Override
    protected void whatToInject(ModelAndView modelAndView, Integer... args ) {

    }
}
