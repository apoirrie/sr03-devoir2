package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.CreateChatClientBody;
import jakarta.validation.Valid;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.*;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class UserListController extends AbstractFragmentController {

    final ChatClientRepository repo ;
    final public List<ChatClient> activeUsers;
    public UserListController(ChatClientRepository repo) {
        super("user_list", "Accueil");
        this.repo = repo;
        this.activeUsers = repo.findByStatus("ACTIF");
    }


    @Override
    protected void whatToInject(ModelAndView modelAndView, Integer... args) {

        int currentPage = (int)args[0];
        int pageSize = (int) args[1];

        Page<ChatClient> userPage = repo.findByStatus("ACTIF",PageRequest.of(currentPage - 1, pageSize));

        modelAndView.addObject("userPage", userPage);

        int totalPages = userPage.getTotalPages();
        modelAndView.addObject("totalPages", totalPages);
        modelAndView.addObject("currentPage", currentPage);

    }


}
