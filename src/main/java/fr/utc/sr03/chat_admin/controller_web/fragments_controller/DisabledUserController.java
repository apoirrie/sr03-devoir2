package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
public class DisabledUserController extends AbstractFragmentController {


    ChatClientRepository repo;
    public DisabledUserController(ChatClientRepository repo) {
        super("disableduser", "Utilisateurs désactivés");
        this.repo = repo;
    }

    @Override
    protected void whatToInject(ModelAndView modelAndView, Integer... args) {
        List<ChatClient> disabledClients = repo.findByStatus("INACTIF");
        modelAndView.addObject("disabledClients", disabledClients);
    }
}
