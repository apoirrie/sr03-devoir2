package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.CreateChatClientBody;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Objects;

@Controller
public class CreateUserController extends AbstractFragmentController{

    final ChatClientRepository repo;
    private final PasswordEncoder encoder;
    public CreateUserController(PasswordEncoder encoder, ChatClientRepository repo) {
        super("createuser", "Ajouter un utilisateur");
        this.encoder =encoder;
        this.repo = repo;
    }

    @Override
    protected void whatToInject(ModelAndView modelAndView, Integer ...args) {
        CreateChatClientBody body = new CreateChatClientBody();
        modelAndView.addObject("createClient", body);
    }

    @PostMapping("/createuser")
    public String createUser(@Valid @ModelAttribute("createClient") CreateChatClientBody createClient, RedirectAttributes attributes, BindingResult validationResult) {
        if(validationResult.hasErrors())
        {
            return "redirect:/admin/createuser";
        }
        if(repo.findByEmail(createClient.getEmail()).isPresent())
        {
            attributes.addFlashAttribute("error", "Email already in use");
        } else if (!Objects.equals(createClient.getPassword(), createClient.getConfirmPassword())) {
            attributes.addFlashAttribute("error", "Passwords do not match");
        }
        else {
            try {
                repo.save(new ChatClient(createClient.getName(), createClient.getSurname(), createClient.getEmail(), encoder.encode(createClient.getPassword()), createClient.getRole() ? "ADMIN" : "USER", "ACTIF"));
                attributes.addFlashAttribute("success", "User added !");
            }catch (Exception e)
            {
                attributes.addFlashAttribute("error", e.getMessage());
            }
        }

        return "redirect:/admin/createuser";
    }
}
