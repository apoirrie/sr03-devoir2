package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.CreateChatClientBody;
import fr.utc.sr03.chat_admin.model.EditChatClientBody;
import jakarta.validation.Valid;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Objects;
import java.util.Optional;

@Controller
public class EditUserController extends AbstractFragmentController {

    final ChatClientRepository repo;
    private final PasswordEncoder encoder;
    private Long userId;
    public EditUserController(PasswordEncoder encoder, ChatClientRepository repo) {
        super("edit_user", "Modifier un utilisateur");
        this.encoder = encoder;
        this.repo = repo;
    }

    @Override
    protected void whatToInject(ModelAndView modelAndView, Integer... args) {
        EditChatClientBody body = new EditChatClientBody();
        modelAndView.addObject("editClient", body);
    }

    @GetMapping("/admin/edit_user/{id}")
    public String editUser(@PathVariable Long id, Model model) {
        userId=id;
        Optional<ChatClient> client = repo.findById(userId);
        if (client.isPresent()) {
            EditChatClientBody editClientBody = new EditChatClientBody();
            ChatClient existingClient = client.get();
            editClientBody.setName(existingClient.getName());
            editClientBody.setSurname(existingClient.getSurname());
            editClientBody.setEmail(existingClient.getEmail());
            editClientBody.setRole(existingClient.getRole().equals("ADMIN"));
            model.addAttribute("editClient", editClientBody);
            Optional<ChatClient> userToEdit = repo.findById(userId);
            model.addAttribute("userToEdit", userToEdit.orElse(null));
            return "fragments/edit_user";
        } else {
            return "redirect:/admin/accueil";
        }
    }

    @PostMapping("/admin/edit_user/{id}")
    public String editUser(@PathVariable Long id, @Valid @ModelAttribute("editClient") EditChatClientBody editClient, RedirectAttributes attributes, BindingResult validationResult) {
        if (validationResult.hasErrors()) {
            return "redirect:/admin/accueil";
        }
            try {
                Optional<ChatClient> existingClientOptional = repo.findById(userId);
                if (existingClientOptional.isPresent()) {
                    ChatClient existingClient = existingClientOptional.get();
                    existingClient.setName(editClient.getName());
                    existingClient.setSurname(editClient.getSurname());
                    existingClient.setEmail(editClient.getEmail());
                    existingClient.setPassword(encoder.encode(editClient.getPassword()));
                    existingClient.setRole(editClient.getRole() ? "ADMIN" : "USER");
                    existingClient.setStatus("ACTIF");
                    repo.save(existingClient);
                } else {
                    repo.save(new ChatClient(editClient.getName(), editClient.getSurname(), editClient.getEmail(), encoder.encode(editClient.getPassword()), editClient.getRole() ? "ADMIN" : "USER", "ACTIF"));
                    attributes.addFlashAttribute("success", "User edited !");
                }
            }
            catch(Exception e)
                {
                    attributes.addFlashAttribute("error", e.getMessage());
                }


            return "redirect:/admin/accueil";
        }
    }
