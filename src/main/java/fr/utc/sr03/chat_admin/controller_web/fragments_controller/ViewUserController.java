package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import org.springframework.ui.Model;
import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Controller
public class ViewUserController extends AbstractFragmentController {

    @Autowired
    ChatClientRepository repo;
    private Long userId;

    public ViewUserController(ChatClientRepository repo) {
        super("view_user", "Consulter un utilisateur");
        this.repo = repo;
    }

    @Override
    protected void whatToInject(ModelAndView modelAndView, Integer... args) {
       /* userId = (Long) args[0];
        Optional<ChatClient> client = repo.findById(userId);
        if (client.isPresent()) {
            modelAndView.addObject("client", client.get());
            Optional<ChatClient> userToView = repo.findById(userId);
            modelAndView.addObject("userToView", userToView.orElse(null));
        }*/
    }


    @GetMapping("/admin/view_user/{id}")
    public String viewUser(@PathVariable Long id, Model model) {
        userId = id;
        Optional<ChatClient> client = repo.findById(userId);
        if (client.isPresent()) {
            model.addAttribute("client", client.get());
            Optional<ChatClient> userToView = repo.findById(userId);
            model.addAttribute("userToView", userToView.orElse(null));
            return "fragments/view_user";

        } else {
            return "redirect:/admin/accueil";
        }
    }

}