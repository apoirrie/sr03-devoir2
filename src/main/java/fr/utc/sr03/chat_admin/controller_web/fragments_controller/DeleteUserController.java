package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.model.ChatClient;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;
import java.util.Optional;

@Controller
public class DeleteUserController  extends AbstractFragmentController {


    ChatClientRepository repo;

    private Long userId;
    public DeleteUserController(ChatClientRepository repo) {
        super("delete_user", "Utilisateur supprimé");
        this.repo = repo;
    }

    @GetMapping("/admin/delete_user/{id}")
    public String deleteUser(@PathVariable Long id, Model model) {
        userId=id;
        Optional<ChatClient> client = repo.findById(userId);
        if (client.isPresent()) {
            model.addAttribute("client", client.get());
            Optional<ChatClient> userDeleted = repo.findById(userId);
            if(userDeleted.isPresent()){
                ChatClient user= userDeleted.get();
                user.setStatus("INACTIF");
                repo.save(user);
            }
            model.addAttribute("userDeleted", userDeleted.orElse(null));
            return "fragments/delete_user";
        } else {
            return "redirect:/admin/user_list";
        }
    }
    @Override
    protected void whatToInject(ModelAndView modelAndView,Integer... args ) {
        Optional<ChatClient> userDeleted = repo.findById(userId);
        modelAndView.addObject("userDeleted", userDeleted.orElse(null));
    }
}
