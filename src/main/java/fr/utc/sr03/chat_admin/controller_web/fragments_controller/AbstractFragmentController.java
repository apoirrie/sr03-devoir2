package fr.utc.sr03.chat_admin.controller_web.fragments_controller;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


public abstract class AbstractFragmentController {
    protected String url;
    protected String title;

    protected org.slf4j.Logger logger = LoggerFactory.getLogger(AbstractFragmentController.class);

    public AbstractFragmentController(String url, String title)
    {
        this.url = url;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public void call(ModelAndView modelAndView, Integer... args)
    {
        modelAndView.addObject("title", getTitle());
        whatToInject(modelAndView, args);
    }
    protected abstract void whatToInject(ModelAndView modelAndView, Integer... args);

}
