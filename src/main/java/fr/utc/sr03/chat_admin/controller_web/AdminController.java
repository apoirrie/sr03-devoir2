package fr.utc.sr03.chat_admin.controller_web;

import fr.utc.sr03.chat_admin.controller_web.fragments_controller.*;
import fr.utc.sr03.chat_admin.dao.ChatClientRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class AdminController {
    private Logger logger = LoggerFactory.getLogger(AdminController.class);
    private final CreateUserController createUserController;
    private final UserListController userListController;
    private final DisabledUserController disabledUserController;
    private final ViewUserController viewUserController;
    private final EditUserController editUserController;
    private final DeleteUserController deleteUserController;
    private final DeleteIndefinitelyController deleteIndefinitelyController;



    public AdminController(CreateUserController createUserController, UserListController userListController,
                           DisabledUserController disabledUserController, ViewUserController viewUserController,
                           EditUserController editUserController, DeleteUserController deleteUserController, DeleteIndefinitelyController deleteIndefinitelyController)
    {

        this.createUserController = createUserController;
        this.userListController= userListController;
        this.disabledUserController= disabledUserController;
        this.viewUserController=viewUserController;
        this.editUserController=editUserController;
        this.deleteUserController=deleteUserController;
        this.deleteIndefinitelyController=deleteIndefinitelyController;


    }

    @GetMapping(value = "/admin/{type}")
    public ModelAndView home(@PathVariable("type") String type, @RequestParam(required = false) Long id) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin");
        modelAndView.addObject("url", type);
        if(id!=null){
            modelAndView.addObject("id",id );
        }

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            String mail_LoggedIn = authentication.getName();
            modelAndView.addObject("mail_LoggedIn", mail_LoggedIn);
        }


        switch (type){
            case "createuser":
                createUserController.call(modelAndView);
                break;
            case "accueil":
                userListController.call(modelAndView,1,7);
                break;
            case "disableduser":
                disabledUserController.call(modelAndView);
                break;

            case "view_user":
                viewUserController.viewUser(id,(Model) modelAndView.getModel());
                logger.info("bonjour");
                break;
            case "edit_user":
                editUserController.editUser(id, (Model) modelAndView.getModel());
                break;
            case "delete_user":
                deleteUserController.deleteUser(id, (Model) modelAndView.getModel());
                break;
            case "delete_indefinitely":
                deleteIndefinitelyController.deleteUserIndefinitely(id, (Model) modelAndView.getModel());
                break;

            default:
                break;
        }
        return modelAndView;
    }

}
