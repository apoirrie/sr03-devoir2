package fr.utc.sr03.chat_admin;

import fr.utc.sr03.chat_admin.config.RsaKeyProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeyProperties.class)
public class ChatAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChatAdminApplication.class, args);
	}

}
