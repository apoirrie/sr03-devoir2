package fr.utc.sr03.chat_admin.model.websocket;

import lombok.Builder;

import java.util.Date;

@Builder
public record ChatMessage(Long senderId, MessageType type, String payload, Date date) {
}
