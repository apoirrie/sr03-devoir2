package fr.utc.sr03.chat_admin.model.reponses;

public class GenericReponseError {
    private String message;

    public GenericReponseError(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
