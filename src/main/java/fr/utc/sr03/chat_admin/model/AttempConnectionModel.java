package fr.utc.sr03.chat_admin.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AttempConnectionModel {

    @Id
    private Long clientId;

    private int wrongCredentialsCount;
}
