package fr.utc.sr03.chat_admin.model.websocket;

public enum MessageType {
    MESSAGE,
    JOIN,
    LEAVE
}
