package fr.utc.sr03.chat_admin.model.websocket;

import fr.utc.sr03.chat_admin.model.LightChatClient;
import lombok.Builder;

import java.util.Date;
import java.util.List;

@Builder
public record UserListMessage(List<LightChatClient> users, Date date) {
}
