package fr.utc.sr03.chat_admin.model;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Setter
@Getter
@Data
public class CreateChatBody {

    @NotEmpty(message = "The name is required")
    private String name;
    @NotEmpty(message = "The description is required")
    private String description;
    @NotEmpty(message = "Date of creation is required")
    private String creationDate;
    @NotEmpty(message = "Date of expiration is required")
    private String expirationDate;
    @NotEmpty(message = "At least one user is requested")
    private List<Long> users;

    public CreateChatBody(String name, String description, String creationDate, String expirationDate, List<Long> users) {
        this.name = name;
        this.description = description;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
        this.users = users;
    }

    @Override
    public String toString() {
        return "CreateChatBody{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", creationDate='" + creationDate + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", users=" + users +
                '}';
    }

}