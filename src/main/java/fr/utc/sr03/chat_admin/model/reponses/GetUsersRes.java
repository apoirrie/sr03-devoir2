package fr.utc.sr03.chat_admin.model.reponses;

import fr.utc.sr03.chat_admin.model.LightChatClient;
import lombok.Getter;

import java.util.List;

@Getter
public class GetUsersRes {

    private final List<LightChatClient> users;

    public GetUsersRes(List<LightChatClient> users) {
        this.users = users;
    }
}
