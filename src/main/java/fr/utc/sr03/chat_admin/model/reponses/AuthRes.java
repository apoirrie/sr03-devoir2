package fr.utc.sr03.chat_admin.model.reponses;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AuthRes {
    private String email;
    private String token;

    public AuthRes(String email, String token) {
        this.email = email;
        this.token = token;
    }

}