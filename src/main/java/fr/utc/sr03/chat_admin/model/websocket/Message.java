package fr.utc.sr03.chat_admin.model.websocket;

import lombok.Builder;

@Builder
public record Message(String username, String message) { }