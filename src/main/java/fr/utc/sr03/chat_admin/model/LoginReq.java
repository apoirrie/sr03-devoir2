package fr.utc.sr03.chat_admin.model;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Data
public class LoginReq {

    @Email(message = "email is not valid")
    private String email;

    @NotEmpty(message = "password is required")
    private String password;

    public LoginReq(String email,String password) {
        this.email = email;
        this.password = password;
    }

}