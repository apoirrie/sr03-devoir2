package fr.utc.sr03.chat_admin.model.reponses;

import fr.utc.sr03.chat_admin.model.LightChat;
import lombok.Getter;

import java.util.List;

@Getter
public class GetChatsRes {

    final List<LightChat> chats;

    public GetChatsRes(List<LightChat> chats) {
        this.chats = chats;
    }
}
