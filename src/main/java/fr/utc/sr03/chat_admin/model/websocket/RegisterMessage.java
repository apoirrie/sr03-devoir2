package fr.utc.sr03.chat_admin.model.websocket;

import fr.utc.sr03.chat_admin.model.LightChatClient;
import lombok.Builder;

@Builder
public record RegisterMessage(LightChatClient client) {
}
