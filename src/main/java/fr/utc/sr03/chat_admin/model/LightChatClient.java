package fr.utc.sr03.chat_admin.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LightChatClient {
    private Long id;
    private String email;
    private String name;
    private String surname;

    public LightChatClient(Long id, String email, String name, String surname) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.surname = surname;
    }
}
