package fr.utc.sr03.chat_admin.model.reponses;

public class CreateChatRes {
    private Long chatId;

    public CreateChatRes(Long chatId) {
        this.chatId = chatId;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }
}
