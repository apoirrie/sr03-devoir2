package fr.utc.sr03.chat_admin.model.reponses;

import fr.utc.sr03.chat_admin.model.LightChatClient;
import lombok.Getter;

@Getter
public class GetMeRes {
    private final LightChatClient user;

    public GetMeRes(LightChatClient user) {
        this.user = user;
    }
}
