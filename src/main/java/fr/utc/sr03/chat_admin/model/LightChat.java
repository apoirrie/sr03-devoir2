package fr.utc.sr03.chat_admin.model;

import jakarta.persistence.Column;
import lombok.Getter;

@Getter
public class LightChat {

    private final Long id;
    private final Long creatorid;

    private final boolean active;
    private final String name;
    private final String description;

    public LightChat(Long id, boolean active, String name, String description, Long creatorid) {
        this.id = id;
        this.active = active;
        this.name = name;
        this.description = description;
        this.creatorid=creatorid;
    }
}
