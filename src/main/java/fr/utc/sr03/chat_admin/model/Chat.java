package fr.utc.sr03.chat_admin.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long creatorId;

    @Column(nullable = false)
    private boolean active;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private Date creation;

    @Column(nullable = false)
    private Date expiration;

    @Column(nullable = false, name = "participants")
    @ElementCollection
    private List<Long> participantsIds;

    public Chat(){};

    public Chat(Long creatorId, boolean active, String name, String description, Date creation, Date expiration,List<Long> participantsIds) {
        this.creatorId = creatorId;
        this.active = active;
        this.name = name;
        this.description = description;
        this.creation = creation;
        this.expiration = expiration;
        this.participantsIds = participantsIds;
    }

}