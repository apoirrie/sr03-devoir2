package fr.utc.sr03.chat_admin.model;
import java.io.Serializable;
import java.util.Objects;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.NonNull;

@Setter
@Getter
@Entity
public class ChatClient implements Serializable {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String email;

    private String name;

    private String surname;

    private String password;

    private String role;

    private String status;


    public ChatClient() {}

    public ChatClient(String name, String surname, String email, String password, String role, String status) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.role = role;
        this.password=password;
        this.status=status;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatClient user = (ChatClient) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(email, user.email) &&
                Objects.equals(name, user.name) &&
                Objects.equals(surname, user.surname) &&
                Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, email, name, surname,role);
    }

    @Override
    public String toString() {
        return "ChatClient{" +
                "email=" + email +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", role='" + role + '\'' +
                ", id='" + id + '\'' +

                '}';
    }
}
