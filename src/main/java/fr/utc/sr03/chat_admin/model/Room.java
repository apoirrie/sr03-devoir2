package fr.utc.sr03.chat_admin.model;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Getter
public class Room {

    private final Long chatId;

    private final List<LightChatClient> connectedUsers = new ArrayList<>();

    public Room(Long chatId)
    {
        this.chatId = chatId;
    }

    public void addConnectedUser(LightChatClient client)
    {
        connectedUsers.add(client);
    }

    public boolean hasUser(Long clientId)
    {
        return connectedUsers.stream().anyMatch(client -> client.getId().equals(clientId));
    }
    public void removeConnectedUser(Long clientId)
    {
        Optional<LightChatClient> first = connectedUsers.stream().filter(client -> client.getId().equals(clientId)).findFirst();
        first.ifPresent(connectedUsers::remove);

    }

}
