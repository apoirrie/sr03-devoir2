package fr.utc.sr03.chat_admin.services;
import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.dao.ConnectionAttempRepository;
import fr.utc.sr03.chat_admin.model.AttempConnectionModel;
import fr.utc.sr03.chat_admin.model.Chat;
import fr.utc.sr03.chat_admin.model.ChatClient;
import fr.utc.sr03.chat_admin.model.LightChatClient;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserService implements UserDetailsService {
    private final ChatClientRepository chatClientRepository;
    private final ConnectionAttempRepository connectionAttempRepository;
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<ChatClient> chatClientOptional = chatClientRepository.findByEmail(email);
        if (chatClientOptional.isEmpty()) {
            throw new UsernameNotFoundException("email %s does not exist".formatted(email));
        } else if (chatClientOptional.get().getStatus().equals("INACTIF")) {
            throw new RuntimeException("This account is disabled");
        }
        ChatClient client = chatClientOptional.get();

        return new User(client.getEmail(), client.getPassword(), getAuthorities(client));
    }



    public LightChatClient toLightChatClient(ChatClient client)
    {
        return new LightChatClient(client.getId(),client.getEmail(),client.getName(),client.getSurname());
    }
    public void handleWrongConnectionAttemp(String email)
    {
        Optional<ChatClient> client = chatClientRepository.findByEmail(email);
        if(client.isPresent())
        {
           Optional<AttempConnectionModel> attemps= connectionAttempRepository.getAttempConnectionModelByClientId(client.get().getId());
           if(attemps.isEmpty())
           {
               connectionAttempRepository.save(new AttempConnectionModel(client.get().getId(),1));
               attemps= connectionAttempRepository.getAttempConnectionModelByClientId(client.get().getId());
           }
           else {
               attemps.get().setWrongCredentialsCount(attemps.get().getWrongCredentialsCount()+1);
           }
           if(attemps.get().getWrongCredentialsCount() > 5)
           {
               client.get().setStatus("INACTIF");
               chatClientRepository.save(client.get());
               attemps.get().setWrongCredentialsCount(0);

           }
            connectionAttempRepository.save(attemps.get());
        }
    }

    private Collection<? extends GrantedAuthority> getAuthorities(ChatClient client) {
        return Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + client.getRole()));
    }


}