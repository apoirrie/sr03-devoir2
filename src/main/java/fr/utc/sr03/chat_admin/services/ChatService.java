package fr.utc.sr03.chat_admin.services;

import fr.utc.sr03.chat_admin.controller_web.AdminController;
import fr.utc.sr03.chat_admin.dao.ChatClientRepository;
import fr.utc.sr03.chat_admin.dao.ChatRepository;
import fr.utc.sr03.chat_admin.model.*;
import fr.utc.sr03.chat_admin.utils.DateHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Slf4j
@Service
public class ChatService {
    private final ChatRepository chatRepository;

    private final ChatClientRepository chatClientRepository;
    private final DateHandler dateHandler;



    public Long extractChatId(String destination) {
        // Extract the chatId from the destination path
        String[] parts = destination.split("/");
        if (parts.length > 3) {
            return Long.valueOf(parts[3]);
        }
        return null;
    }

    public boolean canSubscribeToChat(Long chatId, String email)
    {
        Optional<ChatClient> chatClient = chatClientRepository.findByEmail(email);
        return chatRepository.existsChatByIdAndParticipantsIdsContainingAndExpirationIsAfterAndCreationIsBefore(chatId, Long.valueOf(chatClient.get().getId()), new Date(),new Date());
    }

    public Long createChat(CreateChatBody createChatBody, ChatClient client) throws Exception {
        if(chatRepository.existsByName(createChatBody.getName()))
        {
            throw new Exception("name already exist");
        }
        Optional<Date> creation = dateHandler.dayJsStringToDate(createChatBody.getCreationDate());
        Optional<Date> expiration = dateHandler.dayJsStringToDate(createChatBody.getExpirationDate());
        if(creation.isEmpty() || expiration.isEmpty())
        {
            throw new Exception("Date are not well formatted");
        }
        if(creation.get().after(expiration.get()))
        {
            throw new Exception("Creation is after expiration !");
        }


        List<Long> ids = createChatBody.getUsers();
        ids.add(client.getId());
        Chat chat = new Chat(client.getId(),true, createChatBody.getName(), createChatBody.getDescription(), creation.get(),expiration.get(),ids );
        Chat s = chatRepository.save(chat);
        return s.getId();


        //TODO implement logic
    }

    public void editChat(CreateChatBody createChatBody,Chat chat) throws Exception {
        Optional<Date> creation = dateHandler.dayJsStringToDate(createChatBody.getCreationDate());
        Optional<Date> expiration = dateHandler.dayJsStringToDate(createChatBody.getExpirationDate());
        if(creation.isEmpty() || expiration.isEmpty())
        {
            throw new Exception("Date are not well formatted");
        }
        if(creation.get().after(expiration.get()))
        {
            throw new Exception("Creation is after expiration !");
        }
        chat.setCreation(creation.get());
        chat.setExpiration(expiration.get());
        chat.setName(createChatBody.getName());
        chat.setDescription(createChatBody.getDescription());
        chat.setParticipantsIds(createChatBody.getUsers());
        chatRepository.save(chat);
    }
    public void deleteChat(Long chatId) {
        chatRepository.deleteById(chatId);
    }

    public List<LightChat> getChatOfUser(ChatClient client)
    {
        List<Chat> chatsOfUser = chatRepository.findByParticipantsIdsContaining(client.getId());
        return chatsOfUser.stream().map(chat -> new LightChat(chat.getId(),chat.isActive(),chat.getName(),chat.getDescription(), chat.getCreatorId())).toList();
    }
}