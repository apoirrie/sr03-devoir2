INSERT INTO chatclient (id,email,name,surname,password,role, status) VALUES (123456,'admin@gmail.com','Admin','Test', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'ADMIN', 'ACTIF');
INSERT INTO chatclient (id,email,name,surname,password,role, status) VALUES (789101,'laurianrey@gmail.com','Lauriane','Rey', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF');
INSERT INTO chatclient (id,email,name,surname,password,role, status) VALUES (121314,'antoinepoirrier@gmail.com','Antoine','Poirrier', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'INACTIF');
INSERT INTO chatclient (id,email,name,surname,password,role,status) VALUES (151617,'johndoe@gmail.com','John','Doe', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF');
INSERT INTO chatclient (id,email,name,surname,password,role,status) VALUES (181920,'rickdoe@gmail.com','Rick','Doe', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'INACTIF');
INSERT INTO chatclient (id,email,name,surname,password,role,status) VALUES (212223,'aprildoe@gmail.com','April','Doe', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF');
INSERT INTO chatclient (id,email,name,surname,password,role, status) VALUES (242526,'lisadoe@gmail.com','Lisa','Doe', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF');
INSERT INTO chatclient (id,email,name,surname,password,role,status)
VALUES
    (272829,'alice@gmail.com','Alice','Martin', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (303132,'bob@gmail.com','Bob','Smith', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (333435,'charlie@gmail.com','Charlie','Wilson', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (363738,'david@gmail.com','David','Brown', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (394041,'emma@gmail.com','Emma','Jones', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (424344,'frank@gmail.com','Frank','Davis', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (454647,'grace@gmail.com','Grace','Miller', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (484950,'henry@gmail.com','Henry','Wilson', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (515253,'isabel@gmail.com','Isabel','Moore', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (545556,'jack@gmail.com','Jack','Anderson', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (575859,'kate@gmail.com','Kate','White', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (606162,'leonard@gmail.com','Leonard','Clarkson', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (636465,'mia@gmail.com','Mia','Roberts', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (666768,'nathan@gmail.com','Nathan','Wilson', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (697071,'olivia@gmail.com','Olivia','Hall', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (727374,'peter@gmail.com','Peter','Brown', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (757677,'quinn@gmail.com','Quinn','Robinson', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (787980,'rachel@gmail.com','Rachel','Young', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (808182,'sam@gmail.com','Sam','Harris', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (838485,'tina@gmail.com','Tina','King', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF'),
    (868788,'vince@gmail.com','Vince','Martinez', '$2a$08$Oz3G4zTjafGtaxV5htUBvevGSMpHtqalcvcwPUSl1sot7wvQKCupS', 'USER', 'ACTIF');


INSERT INTO chat (id, creatorid, active, name, description, creation, expiration)
VALUES (11, 123456, true, 'Chat Room 1', 'Description for Chat Room 1', '2023-01-01 00:00:00', '2024-12-31 23:59:59');

INSERT INTO chat (id, creatorid, active, name, description, creation, expiration)
VALUES (22, 121314, true, 'Chat Room 2', 'Description for Chat Room 2', '2023-02-01 00:00:00', '2024-12-31 23:59:59');

INSERT INTO chat (id, creatorid, active, name, description, creation, expiration)
VALUES (33, 123456, true, 'Chat Room 3', 'Description for Chat Room 3', '2023-01-01 00:00:00', '2024-12-31 23:59:59');

INSERT INTO chat (id, creatorid, active, name, description, creation, expiration)
VALUES (44, 123456, true, 'Chat Room 4', 'Description for Chat Room 4', '2023-01-01 00:00:00', '2024-12-31 23:59:59');

INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (11,123456);
INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (33,123456);
INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (44,123456);
INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (11,121314);
INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (11,181920);

INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (22,121314);
INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (22,123456);
INSERT INTO CHAT_PARTICIPANTSIDS (chat_id,participants) VALUES (22,181920);