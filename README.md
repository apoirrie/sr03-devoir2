# sr03-devoir2

## PARTIE ADMIN
Enregistrer le projet et le lancer dans IntelliJ.

Aller dans un navigateur et entrer l’adresse « localhost:8080/admin/accueil ».

**Email admin :** admin@gmail.com

**Mdp :** admin (mot de passe de tout les clients de la base de données mockée)

### POINTS AMÉLIORÉS DEPUIS LA SOUTENANCE :
_Recherche, tri, pagination:_
- Implémentation de la recherche par nom de famille parmi les utilisateurs.
- Implémentation du tri des utilisateurs par ordre alphabétique des noms
- Possibilité de le mettre par ordre alphabétique des prénoms
- Implémentation de la pagination

_Création d’un utilisateur :_

- vérification des champs vides
- Vérification de la longueur du mot de passe

_Trop de tentative de connections ratées_
- Coté React uniquement, un trop grand nombre d'erreur d'authentification désactive le chat

### POINTS À AMÉLIORER:
- L'affichage des chats expirés ou encore non valide. La sécurité est implémenté coté serveur, mais pas encore de gestion coté client
- La gestion des erreurs coté serveur. Pour l'instant c'est le gestionnaire d'erreur par défaut qui est utilisé, il en résulte des erreur dans les logs (sans que cela ne fasse crasher le programme)
- Ajout de test unitaires/intégration, tests de robustesses. 





## PARTIE CLIENT 
Ouvrir le terminal, aller dans le répertoire du projet, puis effectuer les commande : 

1. cd frontend/chat-ut-front
2. npm install
3. npm start

**Mêmes identifiants que partie admin**


