import React, { useContext, createContext, useEffect } from "react";
import { SPRING_END_POINT_API } from "../misc/utils";
import axios, { AxiosError } from "axios";
import { GetUserRes, LightUser } from "../models/UserModels";
import { useGlobalErrorHandler } from "./GlobalErrorHandler";
import { useAuth } from "./AuthProvider";



interface UserHandler {
    users: LightUser[]
}

const UsersContext = createContext<UserHandler | null>(null);

const UserProvider = ({ children }: { children: any }) => {

    const [users, setUsers] = React.useState<LightUser[]>([])
    const authHandler = useAuth()
    const globalErrorHandler = useGlobalErrorHandler()

    useEffect(() => {
        if (!authHandler.authentificated) {
            return
        }
        async function getUsers(): Promise<LightUser[]> {
            if (users.length !== 0) {
                return users
            }
            const res = await axios.get(`${SPRING_END_POINT_API}/getactiveusers`)
            return (res.data as GetUserRes).users
        }
        getUsers().then((users) => {
            setUsers(users)
            console.log("users loaded")
        }).catch((error) => {
            console.error(error)
            if (error instanceof AxiosError) {
                globalErrorHandler.setError(error.message)
                return
            }
            globalErrorHandler.setError("Erreur inconnue")
        })
    }, [authHandler.initialized])








    return (
        <UsersContext.Provider value={{ users }}>
            {children}
        </UsersContext.Provider>
    );

};

export default UserProvider;

export const useUsersHandler = () => {
    return useContext(UsersContext);
};