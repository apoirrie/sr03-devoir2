import React, { useContext, createContext, useState } from "react";
import { Button } from "react-bootstrap";
import { Dialog, DialogActions, DialogContent, DialogTitle, Typography } from "@mui/material";
import { SlideInTransition } from "../components/misc/SimpleConfirmDialog";
import { RouterProps, useNavigate } from "react-router-dom";



interface ErrorHandler {
    setError: (error: string, redirectTo?: string | null) => void
}

const ErrorContext = createContext<ErrorHandler | null>(null);

const GlobalErrorHandlerProvider = ({ children }: { children: any }) => {

    const [errorMessage, setErrorMessage] = useState<string>("")
    const [showError, setShowError] = useState<boolean>(false)
    const [redirectTo, setRedirectTo] = useState<string | null>(null)
    const navigate = useNavigate()
    function onErrorClose() {
        setShowError(false)
        if (!redirectTo) {

            window.location.reload();
        }
        else {
            navigate(redirectTo)
            window.location.reload();
        }
    }

    function setError(message: string, redirectTo: string | null = null) {
        setErrorMessage(message)
        setRedirectTo(redirectTo);
        setShowError(true)
    }

    return (
        <ErrorContext.Provider value={{ setError }}>
            {children}
            <Dialog
                sx={{ '& .MuiDialog-paper': { width: '80%', maxHeight: 435 } }}
                maxWidth="xs"
                open={showError}
                TransitionComponent={SlideInTransition}
            >
                <DialogTitle style={{ backgroundColor: 'rgb(211, 47, 47)', color: "white" }}>Aie Aie Aie</DialogTitle>
                <DialogContent dividers>
                    {errorMessage}
                    <br />
                    <Typography variant="h6">
                        <b>La page va être rechargée.</b>
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button variant="outlined" color="error" onClick={onErrorClose}>Recharger</Button>
                </DialogActions>
            </Dialog>
        </ErrorContext.Provider>
    );

};

export default GlobalErrorHandlerProvider;

export const useGlobalErrorHandler = () => {
    return useContext(ErrorContext) as any as ErrorHandler;
};