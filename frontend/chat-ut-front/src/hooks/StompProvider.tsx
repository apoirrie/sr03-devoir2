import { useContext, createContext, useEffect, useState, useRef } from "react";
import { useAuth } from "./AuthProvider";
import StompAdapter from "../misc/stompAdapter";
import { useGlobalErrorHandler } from "./GlobalErrorHandler";
import { useChatHandler } from "./ChatProvider";
import { LightUser } from "../models/UserModels";
import { Message, UserListMessage } from "../models/ChatModels";



interface StompHandler {
    joinRoom: (chatId: number) => void
    quitRoom: (chatId: number) => void
    sendMessage: (message: string, chatId: number) => void
    messages: Message[]
    connectedUsers: LightUser[]
    connected: boolean
}

const StompContext = createContext<StompHandler | null>(null);

const StompProvider = ({ children }: { children: any }) => {
    const authHandler = useAuth();
    const [messages, setMessages] = useState<Message[]>([])
    const [connected, setConnected] = useState(false);
    const [stompAdapter, setStompAdapter] = useState<StompAdapter>()
    const globalErrorHandler = useGlobalErrorHandler()
    const [connectedUsers, setConnectedUsers] = useState<LightUser[]>([])
    function onError(message: string) {
        globalErrorHandler.setError(message)
    }

    function onConnected(connected: boolean) {
        setConnected(connected)
    }

    function sendMessage(message: string, chatId: number) {
        stompAdapter?.sendMessage(message, chatId)
    }

    function onMessage(message: Message) {
        console.log("message")
        setMessages((msgs) => [...msgs, message])
    }



    function onUsers(usersMessage: UserListMessage) {
        setConnectedUsers((u) => usersMessage.users);
    }

    useEffect(() => {
        if (!authHandler.authentificated) {
            if (stompAdapter != null) {
                stompAdapter.disconnect()
            }
            return
        }
        const adapter = new StompAdapter(authHandler.token!, onError, onConnected, onUsers, onMessage)
        setStompAdapter(adapter)

        return () => {
            stompAdapter?.disconnect()
            setMessages(() => [])
            setConnectedUsers(() => [])
            setStompAdapter(undefined)
        }

    }, [authHandler.authentificated])

    useEffect(() => {
        if (stompAdapter) {
            stompAdapter.connect()
        }
    }, [stompAdapter])

    async function joinRoom(chatId: number) {
        setMessages(() => [])
        setConnectedUsers(() => [])
        stompAdapter?.connectToRoom(chatId)
    }

    async function quitRoom(chatId: number) {

        stompAdapter?.quitRoom(chatId)
        setMessages(() => [])
        setConnectedUsers(() => [])
    }




    return (
        <StompContext.Provider value={{ joinRoom, quitRoom, sendMessage, messages, connectedUsers, connected }}>
            {children}
        </StompContext.Provider>
    );

};

export default StompProvider;

export const useStompHandler = () => {
    return useContext(StompContext) as any as StompHandler;
};