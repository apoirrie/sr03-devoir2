import { useContext, createContext, useState, useLayoutEffect } from "react";
import LoginRequest, { LoginResponse, LoginResponseError } from "../models/AuthModels";
import { SPRING_END_POINT_API } from "../misc/utils";
import axios, { AxiosError } from "axios";
import { GetMeRes, LightUser } from "../models/UserModels";
import { useGlobalErrorHandler } from "./GlobalErrorHandler";



interface AuthHandler {
    token: string | null
    user: LightUser | null
    authentificated: boolean,
    initialized: boolean
    login: (loginRequest: LoginRequest) => Promise<void>
    logout: () => void
}

const AuthContext = createContext<AuthHandler | null>(null);

const AuthProvider = ({ children }: { children: any }) => {

    const globalErrorHandler = useGlobalErrorHandler()

    const [user, setUser] = useState<LightUser | null>(null);
    const [authentificated, setAuthentificated] = useState(false);
    const [initialized, setInitialized] = useState(false);
    const [token, setToken] = useState<string | null>(localStorage.getItem("token"));

    useLayoutEffect(() => {
        async function loadMe() {
            const res = await axios.get(`${SPRING_END_POINT_API}/me`)
            setUser((res.data as GetMeRes).user)
            setAuthentificated(true)
        }

        if (token) {
            axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
            localStorage.setItem("token", token);
            loadMe().then(() => {
                setInitialized(true)
            }).catch(e => {
                delete axios.defaults.headers.common["Authorization"];
                localStorage.removeItem("token");
                setToken(null)
                setAuthentificated(false)
                setUser(null)
                if (e instanceof AxiosError) {
                    if (e.response?.data.message) {
                        globalErrorHandler?.setError(e.response?.data.message)
                    }
                    globalErrorHandler?.setError(e.message)

                }
            })
        }
        else {
            delete axios.defaults.headers.common["Authorization"];
            setAuthentificated(false)

            setUser(null)
            setToken(null)
            localStorage.removeItem("token");
            setInitialized(true)
        }

    }, [token, globalErrorHandler])

    /**
     * Try to get a token from credentials to authentificate future requests
     * @param loginRequest credentials to authentificate user
     */
    const login = async (loginRequest: LoginRequest): Promise<void> => {

        try {
            const response = await axios.post(`${SPRING_END_POINT_API}/authentificate`, loginRequest)

            const res = await response.data as LoginResponse;

            if (res.token) {
                setToken(res.token);
            }

        } catch (e) {
            if (!(e as AxiosError).response) {
                let formattedError: LoginResponseError = { message: (e as AxiosError).message, httpStatus: (e as AxiosError).code as any }
                throw (formattedError)
            }
            throw (((e as AxiosError).response?.data as LoginResponseError))
        }



    };

    const logout = () => {
        setUser(null);
        setToken("");
        localStorage.removeItem("token");
        window.location.reload()
    };

    return (
        <AuthContext.Provider value={{ token: token, user: user, login: login, logout: logout, authentificated: authentificated, initialized: initialized }}>
            {children}
        </AuthContext.Provider>
    );

};

export default AuthProvider;

export const useAuth = () => {
    return useContext(AuthContext) as AuthHandler;
};