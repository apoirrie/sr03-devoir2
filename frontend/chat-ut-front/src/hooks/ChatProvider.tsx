import React, { useContext, createContext, useEffect } from "react";
import { SPRING_END_POINT_API } from "../misc/utils";
import axios, { AxiosError } from "axios";
import { CreateChatBody } from "../models/CreateChatBody";
import { GetMyChatRes, LightChat } from "../models/ChatModels";
import { useAuth } from "./AuthProvider";



interface ChatHandler {
    createChat: (body: CreateChatBody) => Promise<void>,
    updateChat: (body: CreateChatBody, chatId: number) => Promise<void>,
    refreshMyChats: () => Promise<void>
    myChats: LightChat[]
    ownedChats: LightChat[]
    invitedChats: LightChat[]
}

const ChatContext = createContext<ChatHandler | null>(null);

const ChatProvider = ({ children }: { children: any }) => {
    const [myChats, setMyChats] = React.useState<LightChat[]>([])
    const [ownedChats, setOwnedChats] = React.useState<LightChat[]>([])
    const [invitedChats, setInvitedChats] = React.useState<LightChat[]>([])
    const authHandler = useAuth()
    const userId = authHandler.user?.id
    async function createChat(body: CreateChatBody) {
        try {
            await axios.post(`${SPRING_END_POINT_API}/createchat`, body)
            refreshMyChats()
            return

        } catch (e: any) {
            if (e instanceof AxiosError) {
                if (e.response?.data.errors) {
                    throw (e.response?.data.errors[0].defaultMessage)
                }

                else if (e.response?.data.message) {
                    throw e.response?.data.message
                }
                console.log(e)
            }
            throw ("Erreur inconnue")
        }
    }
    async function updateChat(body: CreateChatBody, chatId: number) {
        try {
            await axios.post(`${SPRING_END_POINT_API}/editchat/${chatId}`, body)
            refreshMyChats()
            return

        } catch (e: any) {
            if (e instanceof AxiosError) {
                if (e.response?.data.errors) {
                    throw (e.response?.data.errors[0].defaultMessage)
                }

                else if (e.response?.data.message) {
                    throw e.response?.data.message
                }
                console.log(e)
            }
            throw ("Erreur inconnue")
        }
    }

    async function refreshMyChats() {
        await getMyChats()
        await getInvitedChats()
        await getOwnedChats()
    }

    async function getMyChats(isOwner: boolean = false) {
        try {
            const res = await axios.post(`${SPRING_END_POINT_API}/getmychats`)
            setMyChats((res.data as GetMyChatRes).chats)
        } catch (e) {

        }
    }
    async function getOwnedChats(isOwner: boolean = false) {
        try {
            const res = await axios.post(`${SPRING_END_POINT_API}/getmychats`)
            const chats = (res.data as GetMyChatRes).chats.filter(chat => chat.creatorid === userId);
            setOwnedChats(chats);
        } catch (e) {

        }
    }
    async function getInvitedChats(isOwner: boolean = false) {
        try {
            const invited = myChats.filter(chat => chat.creatorid !== userId);
            setInvitedChats(invited);
        } catch (e) {

        }
    }

    useEffect(() => {
        if (authHandler.authentificated) {
            getMyChats()
        }
    }, [authHandler.authentificated])

    useEffect(() => {
        if (authHandler.authentificated) {
            getOwnedChats()
        }
    }, [authHandler.authentificated])

    useEffect(() => {
        if (authHandler.authentificated) {
            getInvitedChats()
        }
    }, [authHandler.authentificated])







    return (
        <ChatContext.Provider value={{ createChat, updateChat, refreshMyChats, myChats, ownedChats, invitedChats }}>
            {children}
        </ChatContext.Provider>
    );

};

export default ChatProvider;

export const useChatHandler = () => {
    return useContext(ChatContext) as ChatHandler;
};