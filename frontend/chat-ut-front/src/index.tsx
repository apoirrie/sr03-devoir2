import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import {
  createBrowserRouter,
  Outlet,
  RouterProvider,
} from "react-router-dom";
import Root from './root'
import CreateChat from './pages/createchat';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import Login from './pages/login';
import AuthProvider, { useAuth } from './hooks/AuthProvider';
import { ProtectedRoute } from './components/navigation/ProtectedRoute';
import { Helmet } from "react-helmet";
import { RedirectIfAuthentifiedRoute } from './components/navigation/RedirectIfAuthentifiedRoute';
import GlobalErrorHandlerProvider from './hooks/GlobalErrorHandler';
import Saloon from './pages/saloon';
import UserChats from "./pages/mychats";
import UserInvitedChats from "./pages/myinvitations";
import DeleteChat from "./pages/deletechat";
import { useChatHandler } from "./hooks/ChatProvider";
import EditChat from './pages/editchat';

const RenderIfAuthInitialized = function ({ children }: { children: any }) {
  const authHandler = useAuth();

  if (authHandler.initialized) {
    return children
  }
  return (<div>Waiting for auth</div>)
}
//route initialization
const router = createBrowserRouter([
  {
    path: '/',
    element: <GlobalErrorHandlerProvider>
      <AuthProvider>
        <RenderIfAuthInitialized>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <Outlet />
          </LocalizationProvider>
        </RenderIfAuthInitialized>

      </AuthProvider>
    </GlobalErrorHandlerProvider>,
    children: [
      {
        path: "/",
        element: <ProtectedRoute><Root /></ProtectedRoute>,
        children: [
          {
            path: "chat/:chatIdUrl",
            element: <Saloon />,

          },
          {
            path: "mychats",
            element: <UserChats />,
          },
          {
            path: "myinvitations",
            element: <UserInvitedChats />,
          },
          {
            path: "createchat",
            element: <CreateChat />,
          },
          {
            path: "deletechat/:chatId",
            element: <DeleteChat />,
          },
          {
            path: "editchat/:chatIdUrl",
            element: <EditChat />
          }
        ]
      },
      {
        path: "/login",
        element: <RedirectIfAuthentifiedRoute><Login /></RedirectIfAuthentifiedRoute>,
      }
    ]
  },

]);
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);





root.render(
  <>
    <Helmet>
      <title>CHAT'UT</title>
    </Helmet>
    <RouterProvider router={router}></RouterProvider>
  </>
);

