import { Outlet } from "react-router-dom";
import Navbar from "./components/navigation/navbar";
import Sidebar from "./components/navigation/sidebar";
import ChatProvider from "./hooks/ChatProvider";
import UserProvider from "./hooks/UsersProvider";
import StompProvider from "./hooks/StompProvider";
import { Box, CssBaseline } from "@mui/material";

export default function Root() {
    return (
        <ChatProvider>
            <StompProvider>
                <UserProvider>
                    <Box sx={{ display: 'flex', overflow: "hidden" }}>
                        <CssBaseline />
                        <Navbar /><Sidebar />
                        <Box component="main" style={{ paddingTop: "5rem" }} sx={{ flexGrow: 1 }} className="h-100 overflow-y-auto">
                            <Outlet />
                        </Box>
                    </Box>



                </UserProvider>
            </StompProvider>
        </ChatProvider>

    );
}