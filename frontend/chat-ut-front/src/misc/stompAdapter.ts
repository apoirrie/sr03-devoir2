import { Client, IFrame, IMessage, StompSubscription } from "@stomp/stompjs";
import { WEBSOCKET_AUTH_FAILED_ERROR, WEBSOCKET_ENDPOINT } from "./utils";
import { LightUser } from "../models/UserModels";
import { Message, UserListMessage } from "../models/ChatModels";


export default class StompAdapter {
    private client: Client
    connected = false
    joinedRooms: number[] = []
    subscribed: Map<number, StompSubscription[]> = new Map()
    onError: (message: string) => void
    onConnected: (connected: boolean) => void
    onUser: (usersMessage: UserListMessage) => void
    onMessage: (message: Message) => void
    constructor(token: string, onError: (message: string) => void, onConnected = (connected: boolean) => { }, onUser = (usersMessage: UserListMessage) => { }, onMessage = (message: Message) => { }) {
        this.onError = onError
        this.onMessage = onMessage;
        this.onConnected = onConnected;
        this.onUser = onUser;
        this.client = new Client({
            brokerURL: WEBSOCKET_ENDPOINT,
            connectHeaders: {
                Authorization: `Bearer ${token}`
            },
        })
        this.client.onConnect = this.onConnectedSuccess.bind(this)
        this.client.onStompError = this.onStompError.bind(this)
        this.client.onDisconnect = this.onDisconnected.bind(this)
    }

    private onConnectedSuccess(frame: IFrame) {
        this.connected = true
        console.log(frame);
        this.onConnected(this.connected)
    }

    private onDisconnected() {
        this.onConnected(false);
    }

    private onMessageReceived(frame: IFrame) {

        const message = JSON.parse(frame.body) as Message
        this.onMessage(message)
    }

    private onUserUpdateMessage(frame: IFrame) {
        this.onUser(JSON.parse(frame.body) as UserListMessage)
    }

    public quitRoom(chatId: number) {
        if (this.joinedRooms.indexOf(chatId) != -1) {
            console.log("unsubscribing from chat " + chatId)
            if (this.subscribed.has(chatId)) {
                for (const sub of this.subscribed.get(chatId) as StompSubscription[]) {
                    sub.unsubscribe()
                }
                this.subscribed.delete(chatId)
            }
            this.joinedRooms.splice(this.joinedRooms.indexOf(chatId), 1)
        }
    }

    public sendMessage(message: string, chatId: number) {
        this.client.publish({
            destination: `/app/send/${chatId}`,
            body: message,
            skipContentLengthHeader: true,
        })
    }


    public connectToRoom(chatId: number) {
        if (this.joinedRooms.indexOf(chatId) == -1) {
            this.joinedRooms.push(chatId)
            console.log("subscribing to chat " + chatId)
            const sub1 = this.client.subscribe(`/topic/messages/${chatId}`, (message: IMessage) => {
                this.onMessageReceived(message)
            })

            const sub2 = this.client.subscribe(`/topic/users/${chatId}`, (message: IMessage) => {
                this.onUserUpdateMessage(message)
            })
            this.subscribed.set(chatId, [sub1, sub2])
        }
        else {
            console.warn("already connected to chat")
        }

    }

    private onStompError(errorFrame: IFrame) {

        if (errorFrame.headers.message && errorFrame.headers.message === WEBSOCKET_AUTH_FAILED_ERROR) {

            this.onError("Impossible de se connecter au serveur de chat")
        }
        else {
            this.onError(errorFrame.body)
        }
        console.error('Broker reported error: ' + errorFrame.headers['message']);
        console.error('Additional details: ' + errorFrame.body);
        this.client.deactivate()
    }

    async connect() {
        this.client.activate()
    }

    async disconnect() {
        await this.client.deactivate()
        this.connected = false
    }
}