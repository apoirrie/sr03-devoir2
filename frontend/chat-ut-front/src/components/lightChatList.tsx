
import { useEffect, useRef } from "react";
import { useChatHandler } from "../hooks/ChatProvider"
import ChatIcon from "./chaticon";
import { LightChat } from "../models/ChatModels";
import { CircularProgress } from "@mui/material";


/**
 * 
 * Represent all the chats where the user is the creator or invid
 */
export default function LightChatList() {
    const chatHandler = useChatHandler();
    const viewList = useRef<any[]>([])


    useEffect(() => {
        if (chatHandler.myChats) {
            for (const chat of chatHandler!.myChats) {
                viewList.current.push(<ChatIcon key={chat.id} title={chat.name} id={chat.id} />)
                viewList.current.push(<div key={chat.name} className="my-1"></div>)
            }
        }
        else {
            viewList.current = []
            viewList.current.push(<CircularProgress />)
        }
        return () => {
            viewList.current = []
        }
    },
        [chatHandler.myChats])

    return (
        <div className="d-flex flex-column w-100 mt-2" style={{ overflowY: "scroll", height: '200px' }}>

            {viewList.current}
        </div>

    )
}