import { Button, Typography } from "@mui/material";

import { useNavigate } from "react-router-dom";



/**
 * 
 * Represent the delete chat button
 * @returns 
 */
export default function DeleteButton({ id }: { id: number }) {
    const navigate = useNavigate()
    return (
        <Button onClick={() => { navigate(`/deletechat/${id}`) }} variant="contained" color="error"><Typography variant="body2">Supprimer</Typography></Button>
    )
}