import { useState } from "react"
import { useStompHandler } from "../hooks/StompProvider"
import "./css/input.css"
import { IconButton, InputAdornment, TextField } from "@mui/material"
import { Send } from "@mui/icons-material"

/**
 * 
 * Represent the chat input
 * @returns 
 */
export default function ChatInput({ onSend }: { onSend: (message: string) => void }) {
    const [message, setMessage] = useState<string>("")

    function sendMessageRequested() {
        if (message != null) {
            onSend(message)
            setMessage("")
        }
    }

    return (

        <TextField
            label="Envoyer un message"
            fullWidth
            multiline
            value={message}
            onChange={(event) => { setMessage(event.target.value) }}
            rows={2}
            variant="filled"
            InputProps={{
                endAdornment: <InputAdornment position="end">
                    <IconButton onClick={sendMessageRequested}
                    >
                        <Send />
                    </IconButton>
                </InputAdornment>
            }}
        />

    )
}