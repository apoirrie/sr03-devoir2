import { ReactNode } from "react"
import MessageView from "./message";
import "./css/discussion.css"
import { Message } from "../models/ChatModels";
import { CircularProgress } from "@mui/material";

/**
 * 
 * Represent the chat view with all the messages
 * @returns 
 */
export default function Discussion({ messages, mainUser }: { messages: Message[], mainUser: number }) {

    let messagesView: ReactNode[] = []
    for (let i = 0; i < messages.length; i++) {
        const element = messages[i];
        messagesView.push(<MessageView key={i} main={mainUser} message={element} />)

    }
    return (

        <div className="chat-messages overflow-y-scroll">
            {messagesView.length !== 0 ? messagesView : <CircularProgress />}
        </div>

    )
}