import { useLocation } from "react-router-dom"
import "../css/sidebar.css"
import { Box, CircularProgress, Divider, Drawer, IconButton, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Toolbar, Typography } from "@mui/material"
import { Refresh } from "@mui/icons-material"

import { useChatHandler } from "../../hooks/ChatProvider"
import React, { useCallback } from "react"

import ChatIcon from "../chaticon"
import { useAuth } from "../../hooks/AuthProvider"
import { LoadingButton } from "@mui/lab"


/**
 * Represent the sidebar of the application, contains informations about user and available chats
 * @returns 
 */
export default function Sidebar() {
    const chatHandler = useChatHandler();
    const authHandler = useAuth()
    const { authentificated, logout, user } = useAuth();

    /**
     * Refresh the chat list that user can access
     */
    async function refreshChatList() {
        chatHandler?.refreshMyChats()
    }

    const UserDisplay = useCallback(() => {
        if (authHandler.authentificated) {
            return (
                <div className="d-flex w-100 py-1 px-4 flex-column">
                    <span>Email : {authHandler.user?.email}</span>
                    <span>Nom : {authHandler.user?.name}</span>
                    <span>Prénom : {authHandler.user?.surname}</span>
                    <LoadingButton className="mt-2" variant="contained" color="error" onClick={logout}> Se déconnecter </LoadingButton>
                </div>
            )
        }
        return <CircularProgress />
    }, [authentificated, user, logout]);


    return (
        <Drawer
            variant="permanent"

            sx={{
                width: 280,
                flexShrink: 0,
                [`& .MuiDrawer-paper`]: { width: 280, boxSizing: 'border-box' },
            }}
        >
            <Toolbar style={{ paddingTop: "5rem" }} />
            <Box sx={{ overflow: 'auto' }}>
                <Typography className="p-2" variant="h6">Utilisateur</Typography>
                <UserDisplay />
                <Divider className="my-2" />
                <div className="d-flex">
                    <Typography className="p-2" variant="h6">Accès rapide</Typography>
                    <IconButton onClick={refreshChatList} style={{ marginLeft: "auto" }}>
                        <Refresh />
                    </IconButton>
                </div>
                <div className="px-4 w-100">
                    {chatHandler.myChats.map((chat) => (
                        <div key={chat.id} className="my-2">
                            <ChatIcon title={chat.name} id={chat.id} />
                        </div>
                    ))}
                </div>
            </Box>
        </Drawer>
    )



}

