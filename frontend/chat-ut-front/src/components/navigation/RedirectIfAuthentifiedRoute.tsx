import { Navigate } from "react-router-dom";
import { useAuth } from "../../hooks/AuthProvider";


export const RedirectIfAuthentifiedRoute = ({ children }: { children: any }) => {
    const authHandler = useAuth();
    if (authHandler.authentificated) {
        return <Navigate to="/createchat" />;
    }
    return children;
};