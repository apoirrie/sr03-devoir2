import { AppBar, Box, Button, ThemeProvider, Toolbar, Typography, createTheme } from "@mui/material";
import "../css/navbar.css"
import { useNavigate } from "react-router-dom";
interface Link {
    url: string,
    title: string

}
const links: Link[] = [{ url: "/mychats", title: "Mes salons" }, { url: "/createchat", title: "Créer un salon" }, { url: "/myinvitations", title: "Mes invitations" }]
const darkTheme = createTheme({
    palette: {
        mode: 'dark',
        primary: {
            main: '#1976d2',
        },
    },
});


export default function Navbar() {
    const navigate = useNavigate()
    return (
        <ThemeProvider theme={darkTheme}>
            <AppBar color="primary" style={{ height: "5rem" }} sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }} component="nav" >
                <Toolbar className="h-100 d-flex align-items-center">



                    <Typography
                        variant="h6"
                        component="div"
                        sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
                    >
                        CHAT'UT
                    </Typography>
                    <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
                        {links.map((link) => (
                            <Button onClick={() => { navigate(link.url) }} key={link.url} sx={{ color: '#fff' }}>
                                {link.title}
                            </Button>
                        ))}
                    </Box>
                </Toolbar>
            </AppBar>
        </ThemeProvider>
    )
}