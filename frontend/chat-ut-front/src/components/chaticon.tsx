import { Paper, ThemeProvider, Typography, createTheme, styled } from "@mui/material";
import { useNavigate } from "react-router-dom";

const Item = styled(Paper)(({ theme }) => ({
    ...theme.typography.body2,
    display: "flex",
    alignItems: "center",
    paddingLeft: '0.75rem',
    flexShrink: 0,
    color: theme.palette.text.secondary,
    height: 40,
    lineHeight: '40px',
    cursor: "pointer"
}));
const darkTheme = createTheme({ palette: { mode: 'dark' } });


/**
 * 
 * Represent a chat nav item (if you click on it you'll go to the chat )
 * @returns 
 */
export default function ChatIcon({ title, id }: { title: String, id: number }) {
    const navigate = useNavigate()
    return (
        <ThemeProvider theme={darkTheme}>
            <Item onClick={() => { navigate(`/chat/${id}`) }} elevation={1}><Typography variant="body2">{title}</Typography></Item>
        </ThemeProvider>

    )
}