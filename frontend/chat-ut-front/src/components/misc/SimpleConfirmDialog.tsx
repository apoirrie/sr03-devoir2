
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Slide } from "@mui/material"
import { TransitionProps } from "@mui/material/transitions";
import React from "react";


export const SlideInTransition = React.forwardRef(function Transition(
    props: TransitionProps & {
        children: React.ReactElement<any, any>;
    },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

export default function SimpleConfirmDialog({ title, message, open, onClose }: { title: string, message: string, open: boolean, onClose: () => void }) {
    return (
        <Dialog
            sx={{ '& .MuiDialog-paper': { width: '80%', maxHeight: 435 } }}
            maxWidth="xs"
            open={open}
            TransitionComponent={SlideInTransition}
        >
            <DialogTitle style={{ backgroundColor: '#1b5e20', color: "white" }}>{title}</DialogTitle>
            <DialogContent dividers>
                {message}
            </DialogContent>
            <DialogActions>
                <Button variant="contained" color="success" onClick={e => onClose()}>Ok</Button>
            </DialogActions>
        </Dialog>

    )
}