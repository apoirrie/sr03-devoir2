
import { Typography } from "@mui/material"
import { Message, MessageType } from "../models/ChatModels"
import "./css/message.css"
import { hoursMinFromIso } from "../misc/utils"
import { useUsersHandler } from "../hooks/UsersProvider"

/**
 * 
 * Represent a chat message
 * @returns 
 */
export default function MessageView({ main, message }: { main: number, message: Message }) {
    const usersHandler = useUsersHandler()


    function getNameFromUserId(id: number) {
        const user = usersHandler?.users.find((u) => { return u.id == id })
        return `${user?.surname} ${user?.name}`
    }

    if (message.type == MessageType.MESSAGE) {
        return (
            <>
                <Typography style={{ alignSelf: main != message.senderId ? 'start' : 'end' }} className="mt-2" variant="caption">{hoursMinFromIso(message.date)} - {getNameFromUserId(message.senderId)}</Typography>
                <span className=" message-box" style={{ backgroundColor: main != message.senderId ? 'lightgray' : '#17a2b832', alignSelf: main != message.senderId ? 'start' : 'end', whiteSpace: "pre-line" }}><small>{message.payload}</small></span>
            </>

        )
    }
    else if (message.type == MessageType.JOIN) {
        return <><Typography style={{ alignSelf: "center" }} className="mt-2" variant="caption">{hoursMinFromIso(message.date)} -
            <Typography className="align-self-center" variant="caption">User <b>{message.payload}</b> joined the chat</Typography></Typography></>
    }
    else {
        return <><Typography style={{ alignSelf: 'center' }} className="mt-2" variant="caption">{hoursMinFromIso(message.date)} - </Typography><Typography className="align-self-center" variant="caption">User <b>{message.payload}</b> left the chat</Typography></>
    }
}

