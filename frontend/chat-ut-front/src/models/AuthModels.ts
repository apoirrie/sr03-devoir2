

export default interface LoginRequest {
    email: string,
    password: string
}

export interface LoginResponse {
    email: string,
    token: string
}

export interface LoginResponseError {
    httpStatus: string,
    message: string
}