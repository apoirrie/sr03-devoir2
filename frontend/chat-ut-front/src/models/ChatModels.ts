import { LightUser } from "./UserModels"

export interface GetMyChatRes {
    chats: LightChat[]
}

export interface Chat {
    id: number,
    creatorId: number,
    name: string,
    creation: string,
    expiration: string,
    description: string,
    participantsIds: []
    active: boolean
}

export interface LightChat {
    creatorid: number | undefined;
    id: number,
    active: boolean,
    name: string,
    description: string
}

export enum MessageType {
    JOIN = "JOIN",
    LEAVE = "LEAVE",
    MESSAGE = "MESSAGE"
}

export interface Message {
    senderId: number,
    type: MessageType
    payload: string | null
    date: string
}

export interface UserListMessage {
    users: LightUser[],
    date: string
}