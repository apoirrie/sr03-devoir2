

export interface LightUser {
    id: number,
    email: string,
    name: string,
    surname: string
}

export interface GetUserRes {
    users: LightUser[]
}

export interface GetMeRes {
    user: LightUser
}