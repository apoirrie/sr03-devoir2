

export interface CreateChatBody {
    name: String,
    description: string,
    creationDate: string,
    expirationDate: string
    users: number[]
}