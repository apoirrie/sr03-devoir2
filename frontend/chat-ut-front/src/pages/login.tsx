
import { Alert, Box, Paper, Snackbar, TextField, Typography } from "@mui/material"
import "./css/login.css"
import React from "react";
import { validateEmail } from "../misc/utils";
import { useAuth } from "../hooks/AuthProvider";
import { LoginResponseError } from "../models/AuthModels";
import { useNavigate } from "react-router-dom";
import SimpleConfirmDialog from "../components/misc/SimpleConfirmDialog";
import { LoadingButton } from "@mui/lab";

export default function Login() {
    const navigate = useNavigate()


    const [email, setEmail] = React.useState("");
    const [emailError, setEmailError] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [passwordError, setPasswordError] = React.useState("");
    const [open, setOpen] = React.useState(false);
    const [openSuccess, setOpenSuccess] = React.useState(false);
    const [errorLogin, setErrorLogin] = React.useState<string>("")
    const auth = useAuth()
    const [submitting, setSubmitting] = React.useState(false);
    function onLoginSucessClose() {
        setOpenSuccess(false)
        navigate("/findchats")
    }

    /**
     * Check if fields of form are valids
     * @returns if form is valid or not
     */
    function validate(): boolean {
        let valid = true
        if (email === "") {
            setEmailError("Veuillez renseigner un email")
            valid = false
        }
        else if (!validateEmail(email)) {
            setEmailError("L'email n'est pas valide")
            valid = false
        }
        if (password === "") {
            setPasswordError("Veuillez renseigner un mot de passe")
            valid = false
        }
        return valid
    }

    /**
     * Handle submit event for login
     * @param event 
     */
    async function handleSubmit(event: any) {
        event.preventDefault()

        setEmailError("")
        setPasswordError("")
        if (validate()) {
            setSubmitting(true)
            try {
                await auth?.login({ email, password })
                setSubmitting(false)
                setOpenSuccess(true)
            }
            catch (error: any) {
                setSubmitting(false)
                if (error.errors) {
                    setErrorLogin(error.errors[0].defaultMessage)
                }
                else {
                    setErrorLogin((error as LoginResponseError).message)
                }

                setOpen(true)
            }
        }

    }

    return (
        <div className="h-100 w-100 d-flex align-items-center flex-column justify-content-center" style={{ backgroundColor: "#F6F6F6" }}>
            <Typography className="mb-3" variant="h2" ><b>CHAT'UT</b></Typography>
            <Paper style={{ minWidth: "400px" }} className="w-50 p-4 d-flex align-items-center justify-content-center">
                <Box component="form" onSubmit={handleSubmit} noValidate className="d-flex flex-column w-100  mb-4">
                    <Typography variant="h5" gutterBottom className="text-center">
                        Veuillez vous identifier
                    </Typography>
                    <TextField disabled={submitting} error={emailError !== ""}
                        helperText={emailError} onChange={(e) => { setEmail(e.target.value) }} type="email" required label="Email" variant="filled" />

                    <TextField disabled={submitting} className="mt-3" error={passwordError !== ""}
                        helperText={passwordError} onChange={(e) => { setPassword(e.target.value) }} type="password" required label="Mot de passe" variant="filled" />



                    <LoadingButton disabled={submitting} loading={submitting} variant="contained" size="large" type="submit" className="mt-3" color="success">Se connecter</LoadingButton>
                </Box>
            </Paper>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={open}
                onClose={e => { setOpen(false) }}
                autoHideDuration={2000}


            >
                <Alert variant="filled" severity="error">{errorLogin}</Alert>
            </Snackbar>
            <SimpleConfirmDialog title="Vous êtes connecté !" message="Vous allez être redirigé vers la page d'accueil" open={openSuccess} onClose={() => { onLoginSucessClose() }} />
        </div>
    )
}