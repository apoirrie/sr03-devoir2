import { useEffect } from 'react';
import { useChatHandler } from '../hooks/ChatProvider';
import { Link } from 'react-router-dom';
import ChatIcon from "../components/chaticon";
import { Paper, Typography } from '@mui/material';

const UserInvitedChats = () => {
    const { invitedChats, refreshMyChats } = useChatHandler();

    useEffect(() => {
        refreshMyChats();
    }, []);

    return (
        <div className='p-4'>
            <h2>Chats auxquels vous êtes invité</h2>
            {invitedChats.length > 0 ? (

                invitedChats.map(chat => (
                    <Paper key={chat.id} style={{ backgroundColor: '#F6F6F6' }} className="chat-item p-3">
                        <ChatIcon title={chat.name} id={chat.id} />
                        <Typography className='my-2' variant='body1'>{chat.description}</Typography>
                    </Paper>
                ))

            ) : (
                <p>Vous n'êtes invité à aucun chat.</p>
            )}
        </div>
    );
};

export default UserInvitedChats;
