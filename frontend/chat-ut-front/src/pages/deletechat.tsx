import React, {useEffect} from 'react';
import { Button, Paper, Snackbar, Typography } from '@mui/material';
import { Alert } from '@mui/material';
import { useNavigate, useParams } from 'react-router-dom';
import {useChatHandler} from "../hooks/ChatProvider";
import axios from "axios";
import {SPRING_END_POINT_API} from "../misc/utils";



const DeleteChat = () => {
    const { ownedChats, refreshMyChats } = useChatHandler();

    useEffect(() => {
        refreshMyChats();
    }, []);

    const navigate = useNavigate();
    const { chatId } = useParams();

    const [errorDeletingChat, setErrorDeletingChat] = React.useState<string>("");
    const [openError, setOpenError] = React.useState(false);

    const handleDelete = async () => {
        try {
            await axios(`${SPRING_END_POINT_API}/deletechat/${chatId}`, {
                method: 'DELETE',
            });
            refreshMyChats();
            navigate('/mychats');
        } catch (error) {
            console.error('Erreur lors de la suppression du chat :', error);
            setErrorDeletingChat("Erreur lors de la suppression du chat");
            setOpenError(true);
        }
    };

    const handleCloseError = () => {
        setOpenError(false);
    };

    const handleCancel = () => {
        navigate('/mychats');
    };

    return (
        <Paper style={{ padding: '1rem' }}>
            <Typography variant="h6">Confirmez la suppression du chat :</Typography>
            <Typography variant="body1">Êtes-vous sûr de vouloir supprimer ce chat ? Cette action est irréversible.</Typography>
            <div style={{ marginTop: '1rem' }}>
                <Button variant="contained" color="error" onClick={handleDelete}>Oui</Button>
                <Button variant="contained" onClick={handleCancel}>Non</Button>
            </div>
            <Snackbar
                anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
                open={openError}
                onClose={handleCloseError}
                autoHideDuration={6000}
            >
                <Alert severity="error" onClose={handleCloseError}>
                    {errorDeletingChat}
                </Alert>
            </Snackbar>
        </Paper>
    );
};

export default DeleteChat;
