import { DateTimePicker } from "@mui/x-date-pickers";
import "./css/createchat.css"

import { Alert, Box, Chip, CircularProgress, FormControl, FormHelperText, MenuItem, OutlinedInput, Paper, Select, SelectChangeEvent, Snackbar, TextField, Theme, Typography, useTheme } from "@mui/material";
import dayjs, { Dayjs } from "dayjs";
import React, { useEffect } from "react";
import { useChatHandler } from "../hooks/ChatProvider";
import { CreateChatBody } from "../models/CreateChatBody";
import SimpleConfirmDialog from "../components/misc/SimpleConfirmDialog";
import { LoadingButton } from "@mui/lab";
import { useUsersHandler } from "../hooks/UsersProvider";
import { LightUser } from "../models/UserModels";
import { useAuth } from "../hooks/AuthProvider";
import { useParams } from "react-router-dom";
import { Chat, LightChat } from "../models/ChatModels";
import axios from "axios";
import { SPRING_END_POINT_API } from "../misc/utils";

/* #region Styling */
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};


function getStyles(user: LightUser, ids: readonly number[], theme: Theme) {
    return {
        fontWeight:
            ids.indexOf(user.id) === -1
                ? theme.typography.fontWeightRegular
                : theme.typography.fontWeightMedium,
    };
}

function getNameFromId(id: number, users: LightUser[]) {
    const user = users.find((u) => { return u.id === id })
    return `${user!.name} ${user!.surname}`
}
/* #endregion */



export default function EditChat() {
    const { chatIdUrl } = useParams()
    const chatId = parseInt(chatIdUrl as string);
    const theme = useTheme();

    const chatHandler = useChatHandler()
    const usersHandler = useUsersHandler();
    const users = usersHandler?.users;
    const authHandler = useAuth();
    const me = useAuth()?.user;
    //display stuff
    const [open, setOpen] = React.useState(false);
    const [errorCreatingChat, setErrorCreatingChat] = React.useState<string>("")

    const [openSuccess, setOpenSuccess] = React.useState(false);
    const [submitting, setSubmitting] = React.useState(false);

    //form stuff
    const [participants, setParticipants] = React.useState<number[]>([]);
    const [participantsError, setParticipantsError] = React.useState<string>("");

    const [chatName, setChatName] = React.useState("");
    const [nameError, setChatNameError] = React.useState("");

    const [chatDescription, setChatDescription] = React.useState("");
    const [chatDescriptionError, setChatDescriptionError] = React.useState("");

    const [chatStartDate, setChatStartDate] = React.useState<Dayjs | null>(null);
    const [chatStartDateError, setChatStartDateError] = React.useState("");

    const [chatEndDate, setChatEndDate] = React.useState<Dayjs | null>(null);
    const [chatEndDateError, setChatEndDateError] = React.useState("");

    useEffect(() => {
        if (authHandler.authentificated) {
            axios.post(`${SPRING_END_POINT_API}/getchat/${chatId}`).then((res) => {
                const chat: Chat = res.data as Chat;
                setChatName(chat.name)
                setChatDescription(chat.description)
                setChatEndDate(dayjs(chat.expiration))
                setChatStartDate(dayjs(chat.creation))
                setParticipants(chat.participantsIds)
            })
        }
    }, [authHandler.initialized])

    function resetForm() {
        setChatName("")
        setChatDescription("")
        setChatEndDate(null)
        setChatStartDate(null)
        setParticipants([])
    }

    const isFormValid = function (): boolean {
        let valid = true
        if (chatName === "") {
            setChatNameError("Veuillez entrer un nom")
            valid = false
        }
        if (chatDescription === "") {
            setChatDescriptionError("Veuillez entrer une description")
            valid = false
        }

        if (chatStartDate === null) {
            setChatStartDateError("Veuillez saisir une date")
            valid = false;
        }
        if (chatEndDate === null) {
            setChatEndDateError("Veuillez saisir une date")
            valid = false;
        }
        if (chatStartDate !== null && chatEndDate !== null && chatStartDate > chatEndDate) {
            setChatEndDateError("La date de fin doit être ultérieure à la date de début")
            valid = false
        }
        if (participants.length === 0) {
            setParticipantsError("Vous devez choisir au moins un participant")
            valid = false
        }
        return valid
    }

    const handleSubmit = async function (event: any) {
        event.preventDefault()
        //Reset errors
        setChatNameError("")
        setChatDescriptionError("")
        setChatStartDateError("")
        setChatEndDateError("")
        setParticipantsError("")
        //check for error
        if (isFormValid()) {
            setSubmitting(true)
            const createChatBody: CreateChatBody = {
                name: chatName,
                description: chatDescription,
                creationDate: chatStartDate!.toISOString(),
                expirationDate: chatEndDate!.toISOString(),
                users: participants

            }
            try {
                await chatHandler?.updateChat(createChatBody, chatId)
                setOpenSuccess(true)
                setSubmitting(false)
            } catch (error) {
                setErrorCreatingChat(error as string)
                setOpen(true)
                setSubmitting(false)
            }
        }


    }

    const handleChange = (event: SelectChangeEvent<typeof participants>) => {
        const {
            target: { value },
        } = event;

        setParticipants(typeof value === 'string' ? value.split(',').map((s) => { return parseInt(s) }) : value)
        /* setParticipants(
            // On autofill we get a stringified value.
             ? value.split(',') : value,
        ); */
    };

    if (!users || !me) {
        return (<div style={{ backgroundColor: "#F6F6F6" }} className="p-1 d-flex align-items-center justify-content-center w-100 overflow-y-auto">
            <CircularProgress />
        </div>)
    }
    return (<div style={{ backgroundColor: "#F6F6F6" }} className="p-1 d-flex align-items-start justify-content-center w-100 overflow-y-auto">
        <Paper className="d-flex mt-5 mb-5 p-5">
            <Box component="form" onSubmit={handleSubmit} noValidate className="d-flex flex-column">
                <Typography variant="h4" gutterBottom className="text-center">
                    Édition du salon
                </Typography>
                <TextField value={chatName} error={nameError !== ""}
                    disabled={submitting}
                    helperText={nameError} onChange={(e) => { setChatName(e.target.value) }} id="filled-basic" required label="Nom du chat" variant="filled" />
                <TextField
                    id="filled-multiline-static"
                    label="Description"
                    value={chatDescription}
                    disabled={submitting}
                    multiline
                    rows={4}
                    className="mt-3"
                    variant="filled"
                    error={chatDescriptionError !== ""}
                    helperText={chatDescriptionError}

                    required
                    onChange={(e) => { setChatDescription(e.target.value) }}
                />
                <div className="d-flex">
                    <DateTimePicker disabled={submitting} value={chatStartDate} onChange={(value) => { setChatStartDate(value) }} slotProps={{ textField: { variant: 'filled', required: true, error: chatStartDateError !== "", helperText: chatStartDateError } }} className="mt-3  w-100" ampm={false} minDateTime={dayjs()} label="Date de création" />
                    <div className="mx-1"></div>
                    <DateTimePicker disabled={submitting} value={chatEndDate} onChange={(value) => { setChatEndDate(value) }} slotProps={{ textField: { variant: 'filled', required: true, error: chatEndDateError !== "", helperText: chatEndDateError } }} className="mt-3 w-100" ampm={false} minDateTime={dayjs()} label="Date de fin" />
                </div>
                <Typography variant="body1" className="my-2">Utilisateurs à ajouter * :</Typography>
                <FormControl error={participantsError !== ""}>
                    <Select
                        labelId="demo-multiple-chip-label"
                        id="demo-multiple-chip"
                        multiple
                        variant="filled"
                        value={participants}
                        disabled={submitting}

                        onChange={handleChange}
                        input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
                        renderValue={(selected) => (
                            <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                                {selected.map((id) => (

                                    <Chip key={id} label={getNameFromId(id, users)} />
                                ))}
                            </Box>
                        )}
                        MenuProps={MenuProps}
                    >
                        {users.filter((u) => { return (u.id !== me!.id) }).map((user: LightUser) => (
                            <MenuItem
                                key={user.id}
                                value={user.id}
                                style={getStyles(user, participants, theme)}
                            >
                                {user.name + ' ' + user.surname}
                            </MenuItem>
                        ))}
                    </Select>
                    {participantsError !== "" && <FormHelperText>{participantsError}</FormHelperText>}
                </FormControl>
                <LoadingButton loading={submitting} disabled={submitting} variant="contained" size="large" type="submit" className="mt-3" color="primary">Éditer le chat</LoadingButton>
            </Box>
        </Paper>
        <Snackbar
            anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
            open={open}
            onClose={e => { setOpen(false) }}
            autoHideDuration={2000}


        >
            <Alert variant="filled" severity="error">{errorCreatingChat}</Alert>
        </Snackbar>
        <SimpleConfirmDialog title="Niquel !" message="Le chat a été modifié" open={openSuccess} onClose={() => { setOpenSuccess(false) }} />
    </div>)
}