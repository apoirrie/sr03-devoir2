import { useEffect } from 'react';
import { useChatHandler } from '../hooks/ChatProvider';
import DeleteButton from '../components/deletebutton';

import ChatIcon from "../components/chaticon";
import "./css/myChats.css"
import { useNavigate } from 'react-router-dom';
import { Button, Paper, Typography } from '@mui/material';

const UserChats = () => {
    const { ownedChats, refreshMyChats } = useChatHandler();
    const navigate = useNavigate()
    useEffect(() => {
        refreshMyChats();
    }, []);


    return (
        <div>

            {ownedChats.length > 0 ? (
                <div className='p-4'>
                    <h2>Vos Chats</h2>

                    {ownedChats.map(chat => (
                        <Paper key={chat.id} style={{ backgroundColor: '#F6F6F6' }} className="chat-item p-3">
                            <ChatIcon title={chat.name} id={chat.id} />
                            <Typography className='my-2' variant='body1'>{chat.description}</Typography>
                            <div className='d-flex'>
                                <DeleteButton id={chat.id} />
                                <div className='mx-2'></div>
                                <Button variant='contained' color='primary' onClick={() => {
                                    navigate(`/editchat/${chat.id}`)
                                }} >Éditer</Button>
                            </div>

                        </Paper>
                    ))}

                </div>
            ) : (
                <p>Vous n'avez créé aucun chat.</p>
            )}
        </div>
    );
};

export default UserChats;
