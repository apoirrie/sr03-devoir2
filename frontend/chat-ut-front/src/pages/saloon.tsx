import { useParams } from "react-router-dom";
import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { useStompHandler } from "../hooks/StompProvider";
import { Avatar, Box, Button, Chip, CircularProgress, Divider, Drawer, Toolbar, Typography } from "@mui/material";
import Discussion from "../components/discussion";
import { useAuth } from "../hooks/AuthProvider";
import { Message } from "../models/ChatModels";
import ChatInput from "../components/input";
import { useGlobalErrorHandler } from "../hooks/GlobalErrorHandler";
import { Wifi } from "@mui/icons-material";
import { stringAvatar } from "../misc/utils";

export default function Saloon() {
    const { chatIdUrl } = useParams();
    const globalErrorHandler = useGlobalErrorHandler()

    const chatId = parseInt(chatIdUrl as string)
    if (isNaN(chatId)) {
        globalErrorHandler.setError("Le nom du chat est invalide", "/mychats")
    }
    const stompHandler = useStompHandler()
    const authHandler = useAuth()
    useEffect(() => {
        try {
            //TODO handle is no id is provided
            if (stompHandler.connected) {
                stompHandler.joinRoom(chatId)
            }
        } catch (error) {
            console.error(error)
        }
        return () => {
            stompHandler.quitRoom(chatId)
        }
    }, [chatId, stompHandler.connected])

    function onSendMessageRequested(message: string) {
        console.log("azd")
        stompHandler.sendMessage(message, chatId)
    }

    return (
        <div style={{ backgroundColor: "#F6F6F6", height: "calc(100vh - 5rem)" }} className="p-1 d-flex align-items-start justify-content-center w-100 overflow-y-auto">
            <div className="d-flex flex-column w-75 h-100">
                {
                    authHandler.user ? <Discussion messages={stompHandler.messages} mainUser={authHandler.user!.id} /> : <CircularProgress />
                }
                <div style={{ marginTop: "auto" }} className="px-3 w-100 d-flex">
                    <ChatInput onSend={onSendMessageRequested} />
                </div>
            </div>
            <Toolbar className="h-100 w-25 bg-white d-flex flex-column">
                <div className="d-flex mt-2 align-items-center">
                    <Wifi style={{ marginRight: "0.25rem" }} /><Typography variant="body1"><b>Utilisateurs connectés</b></Typography>
                </div>
                <Divider className="my-2" variant="middle" />
                {stompHandler.connectedUsers.map((user) => {
                    return (
                        <Chip className="w-100 mt-2" sx={{
                            height: 'auto',
                            '& .MuiChip-label': {
                                whiteSpace: 'normal',
                                padding: "1rem",
                            },
                            display: 'flex',
                            alignItems: "center",
                            justifyContent: "start"
                        }} key={user.id} avatar={
                            <Avatar style={{ color: "white", height: "34px", width: "34px" }}  {...stringAvatar(`${user.surname} ${user.name}`)} />} label={`${user.surname} ${user.name}`} />
                    )
                })}
            </Toolbar>
        </div>

    )
}